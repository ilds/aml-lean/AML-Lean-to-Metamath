
$[ matching-logic-propositional.mm $]

$v  _X999 _x0 $.
$d  _X999 _x0 $.

_X999-is-set-var $f #SetVariable _X999 $.
_x0-is-element-var $f #ElementVariable _x0 $.

test $p |- ( \imp _x0 _x0 ) $=  _x0-is-element-var element-var-is-var var-is-pattern imp-reflexivity $.
