\documentclass{report}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\usepackage{enumerate}
\usepackage[margin=1in]{geometry}
\usepackage[hidelinks]{hyperref}

\newtheorem{theorem}{Theorem}
\newtheorem*{theorem*}{Theorem}

\renewcommand{\phi}{\varphi}
 
\definecolor{keywordcolor}{rgb}{0.7, 0.1, 0.1}   % red
\definecolor{commentcolor}{rgb}{0.4, 0.4, 0.4}   % grey
\definecolor{symbolcolor}{rgb}{0.0, 0.1, 0.6}    % blue
\definecolor{sortcolor}{rgb}{0.1, 0.5, 0.1}      % green
\definecolor{errorcolor}{rgb}{1, 0, 0}           % bright red
\definecolor{stringcolor}{rgb}{0.5, 0.3, 0.2}    % brown
\def\lstlanguagefiles{lstlean.tex}
\lstset{
    escapeinside={(*}{*)},
    language=lean,
    breaklines=true
}

\lstdefinelanguage{none}{}

\newcommand{\red}[1]{{\color{red}#1}}

 
\title{Extracting Matching Logic proofs from Lean to Metamath \\ Technical report}
\author{
  Horațiu Cheval$^a$ \\ 
}
\date{
  {\small $^a$Institute for Logic and Data Science, Bucharest} \\[2ex]
  {\small February 12, 2023}
}

\begin{document}
  \maketitle

  \chapter{Introduction}

  This tehnical report describes the implementation of a system 
  for extracting Applicative Matching Logic \cite{CheLucRos21} proofs formalized in Lean 
  to Metamath proof objects. The Lean formalization of Matching Logic that is used here is the one 
  found at \cite{CheMac22}, while the Metamath formalization is found at \url{https://github.com/kframework/proof-generation/tree/main/theory}. 
  The project contains two main approaches of achieving this, which we call the concrete and generic extraction. 
  The problem of extracting formalized Matching Logic proofs to Metamath proof objects 
  is also studied as part of the formalization of Matching Logic in Coq \cite{BerCheHorPenTus22}.

  \chapter{Emitting Metamath}\label{chap:emitting-metamath}

  \section{MMBuilder.lean}
  This file contains constructs intending to represent Metamath objects. 
  The constructs can be easily exported straight into a Metamath file
  and they are designed to be the final stage of a pipeline that emits Metamath. 
  The constructs are \textbf{not} intended to represent a generic Metamath file,
  but were defined only to be used for the encoding of Matching Logic in Metamath code. 
  The extent to which such representation is useful in general is not the subject of this 
  technical report nor of the implementation described therein. 


  The most important type defined is arguably \lstinline{MMProof}, which holds a Metamath proof. 
  \begin{lstlisting}
inductive MMProof where 
| app : String → List MMProof → MMProof 
  \end{lstlisting}
  While in Metamath a proof can be simply defined to be a sequence of so-called labels 
  that are intended to be read in postfix notation, 
  the type \lstinline{MMProof} holds the proof parsed, so that we do not discard the information of 
  what proof a subproof corresponds to. 
  It is of course trivial, given such a proof representation of type \lstinline{MMProof} to ``flatten''
  it to an actual Metamath proof made up of a sequence of labels.  

  Beyond a proof, in order to emit a valid Metamath theorem, several other declaration are needed in the generated file.
  These are lists of
  \begin{itemize}
    \item Metamath variables (\lstinline{$v} statements);
    \item floatings hypotheses (\lstinline{$f} statements); 
    \item essential hypotheses (\lstinline{$e} statements);
    \item axioms (\lstinline{$a} statements).
  \end{itemize}

  The structure \lstinline{Env} is designed to contain these associated objects.
  \begin{lstlisting}
structure Env where 
  metavars : List String
  floatings : List Hypothesis 
  essentials : List Hypothesis
  assumptions : List Hypothesis
  \end{lstlisting}

  Here, a \lstinline{Hypothesis} is just a structure 
  \begin{lstlisting}
structure Hypothesis where 
  label : String 
  stmt : String 
  kind : HypKind
  \end{lstlisting}
  where \lstinline{Hypkind} is an enumeration with possible values 
  \lstinline{floating}, \lstinline{essential}, \lstinline{assumption}, 
  representing the different kinds of hypotheses. 
  Thus, the Metamath code 
  \begin{verbatim}
sg-is-symbol $f #Symbol sg $. 
  \end{verbatim}
  is encoded by a \lstinline{Hypothesis} with 
  \lstinline{label := "sg-is-symbol"}, \lstinline{stmt := "#Symbol sg"}, \lstinline{kind := floating}. 
  We could of course have an encoding of statements more informing than just strings 
  (like a list of tokens or even remembering information about the leading ``predicate'' like \lstinline{Symbol} here)
  but we deemed it not necessary for the time being.

  A series of convenience functions for manipulating environments are defined in this file.
  These are 
  \begin{lstlisting}
addFloating (env : Env) (label : String) (stmt : Claim) : Env
  \end{lstlisting}
  and the like, which add an object to an environment.

  A remark to be made is that we do not want duplicates in an environment,
  so we could ideally use a more efficient data structure for this, 
  instead of using lists and arrays and erasing their duplicates after we are done constructing them,
  as we do for the moment. This is not a high-priority issue.

  A \lstinline{MMTheorem} puts together a statement, its proof, its corresponding environment and its name:
  \begin{lstlisting}
structure MMTheorem where 
  label : String 
  env : Env 
  proof : MMProof 
  conclusion : String 
  \end{lstlisting}

  The main function related to \lstinline{MMTheorem} is \lstinline{MMTheorem.toMM : MMTheorem → String} 
  which converts the structured representation of the theorem into a string writeable to a Metamath file. 

  We would like to be able to export multiple theorems in one single file, and allow them to interact with each other
  (e.g. a proof of a theorem may refer to a previously extracted theorem in the same file). 
  This cannot be accomplished by just extracting the theorems one after the other and concatenating the results,
  because some information needs to be shared among them, like the variable declarations and floating hypotheses. 
  Thus, the structure \lstinline{MMFile} is insteded to represent a Metamath file containing more than just one theorem,
  with a merged environment of its theorems list, but the implementation thereof is mostly just a placeholder at the time of writing.







  \chapter{Translating concrete proofs}\label{chap:concrete-extraction}


  Let us first discuss informally the problem of extracting a Matching Logic proof from the 
  Lean formalization to the Metamath formalization. 
  Generally speaking, we want a function 

  \begin{lstlisting}
extractToMetamath (φ) : Proof φ → MMProof 
  \end{lstlisting}
  which, for any \lstinline{φ : Pattern 𝕊}, takes a proof of \lstinline{φ} and 
  produces a Metamath proof of the same pattern in its corresponding Metamath representation. 
  Consider then the Matching Logic theorem 
\begin{lstlisting}
  def implSelf (φ) : Proof (φ ⇒ φ) := ... 
\end{lstlisting}
  stating that, for any pattern \lstinline{φ}, we have that \lstinline{φ ⇒ φ}. 
  Note that \lstinline{implSelf} is \textbf{not} of type \lstinline{Proof},
  but is a dependent function of type \lstinline{∀ φ : Pattern, Proof (φ ⇒ φ)}. 
  If we were to call the supposed extraction function on this theorem, \lstinline{extractToMetamath implSelf},
  we would see that the expression does not typecheck,
  because \lstinline{extractToMetamath} expects an argument of type \lstinline{Pattern},
  which \lstinline{implSelf} is not. 
  Note that neither would it make sense for \lstinline{implSelf} to be the second argument of 
  \lstinline{extractToMetamath} of the proof \lstinline{Proof φ}, as now \lstinline{φ} does not exist. 

  The only way for the types to match is to give a value to \lstinline{φ}, say \lstinline{φ := ⊥},
  so that we have 
\begin{lstlisting}
  implSelf ⊥ : Proof (⊥ ⇒ ⊥)
\end{lstlisting}
  and now we can call 
\begin{lstlisting}
  extractToMetamath ⊥ (implSelf ⊥) 
\end{lstlisting}
  to get a Metamath proof of \lstinline{⊥ ⇒ ⊥}, 
  and similarily for any choice of \lstinline{φ}.
  Thus, for any concrete instantiation of the argument \lstinline{φ}, 
  we get a different Metamath proof of \lstinline{φ ⇒ φ} for that choice of \lstinline{φ}.
  This is what we mean by a concrete translation, which constitutes one of the two main approaches we follow. 

  Note that this is fundamentally different from translating the generic Lean proof of 
  \lstinline{φ ⇒ φ} to a generic Metamath proof of \lstinline{φ ⇒ φ},
  which is the approach we describe in Chapter \ref{chap:generic-extraction}.


  \section{ToMMClaim, ToMMProof.lean and ToMMProofProof.lean}
  In line with the previous discussion, in this file we aim to define a function  
  \begin{lstlisting}
def Proof.toMMProof {Γ : Premises 𝕊} {φ : Pattern 𝕊} : Proof Γ φ → MMProof := ...
  \end{lstlisting}
  that translates terms of type \lstinline{Proof} into Metamath proofs, 
  represented as terms of type \lstinline{MMProof} (described in Chapter \ref{chap:emitting-metamath}).
  The function is defined by recursion on proofs. 
  We do not give here the full implementation because it is too long. 
  The actual implementation is quite a bit more complicated, even if we speak about its type, 
  because it also has to generate other side-information which is not strictly contained in the proof, 
  like an environment and a list of statements deferred to an external prover.

  To give a simplified example, in the case \lstinline{modusPonens}, the function is defined by
  \begin{lstlisting}
| @modusPonens _ _ φ ψ h₁ h₂ => .app "proof-rule-mp" 
  [φ.toMMProof, ψ.toMMProof, h₁.toMMProof, h₂.toMMProof] 
  \end{lstlisting}
  % Also there are several configuration options which we omit for simplicity.

  This says that an application of the proof rule \lstinline{modusPonens} from Lean is mapped to the proof rule called 
  \texttt{proof-rule-mp} in Metamath applied to the respective translations of the two patterns involved,
  \lstinline{φ} and \lstinline{ψ}, and of the required hypotheses \lstinline{h₁} and \lstinline{h₁}. 
  This example already shows an inherent difference between the Lean and Metamath representations:
  while in Lean patterns are simply defined by some construction rules and can then be used as such,
  in Metamath one writes a \emph{proof} that an expression is a pattern,
  and it is through that proof that a pattern is referred to in a larger proof. 
  Thus, a pattern exists both as a statement and as a proof in Metamath. 
  For example, the pattern \lstinline{⊥ ⇒ ⊥} is written as \texttt{( imp bot bot )}
  in statements, whereas in proofs one writes it as a proof of its ``patternness'': 
  \texttt{bot-is-pattern bot-is-pattern imp-is-pattern}.
  The same is true for other objects which are present in Lean proofs as arguments (variables, positivity conditions, freshness conditions, etc.).
  Thus, we define a typeclass called \lstinline{ToMMProof} with a field called \lstinline{toMMProof},
  which all Lean types that we want to export as Metamath proofs should be made instances of.
\begin{lstlisting}
class ToMMProof (α : Type)  where 
  toMMProof : α → MMProof
\end{lstlisting}

  We first define instances for the types \lstinline{Pattern}, \lstinline{EVar}, \lstinline{SVar}, \lstinline{Proof} 
  and we assume that \lstinline{𝕊} (the type variable representing the type of symbols) is an instance.

  Defining the instance for variables is easy. Recall that element and set variables are given 
  simply through natural number indices, so we can choose a convention for translating them. 
  We map the \lstinline{EVar} (resp. \lstinline{SVar}) with index $i$ to 
  the Metamath name \texttt{x\textit{i}} (resp. \texttt{{X\textit{i}}}) and to the floating assumption,
  and thus the proof, labeled with \texttt{x\textit{i}-is-evar} (resp. \texttt{X\textit{i}-is-svar}).
  It is also straightforward to define \lstinline{toMMProof} for patterns, by recursion. 
  For example, the representation of an implication as a Metamath proof is given by 
  \begin{lstlisting}
| φ ⇒ ψ => .app "imp-is-pattern" [φ.toMMProof, ψ.toMMProof]
  \end{lstlisting}
  We also choose not to unfold patterns up to primitive constructors, 
  but to stop at some Metamath representable derived connectives. 
  These are conjunction, disjunction, negation, top and the universal and $\nu$ binders. 
  This means that, for example, \lstinline{φ ⋁ ψ} will be exported straight away as 
  \lstinline{.app "or-is-pattern" [φ.toMMProof, ψ.toMMProof]}, 
  and \emph{not} first unfolded to \lstinline{∼φ ⇒ ψ} and then exported according to this.

  As we've already said, patterns can also be exported as statements 
  and this is done by the \lstinline{Pattern.toMMClaim : Pattern 𝕊 → String} function.

  \section{MLITP.lean}
  Beyond patterns and variables, the other kinds of objects which can appear as arguments in a \lstinline{Proof} are the of:
  \begin{itemize}
    \item substitutability hypotheses (the function \lstinline{substiutableforEvarIn} and the like);
    \item positivity hypotheses (the inductive predicate \lstinline{Positive} or the function \lstinline{positive});
    \item freshness hypotheses(the function \lstinline{isFreeEvar} and the like);
    \item tautology hypotheses(the predicate \lstinline{Tautology});
    \item application contexts (terms of type \lstinline{AppContext}).
  \end{itemize}

  These are inherent to the proof rules that require them and thus are also present in the Metamath formalization,
  though their precise formulation may differ, which makes it challenging to export proofs of such conditions 
  to Metamath. However, the much more serious blocking point is that these conditions talk about the 
  formalized syntax of Matching Logic and are not part of the embedded Matching Logic proof system and syntax. 
  Thus, they live at the meta-level with respect to the Matching Logic formalization: 
  they are in general just arbitrary Lean terms which could be defined in any possible way 
  as long as they have the appropriate types. Due to these reasons, there is not much hope 
  of extracting any proof of such a condition to Metamath, as that would in principle require 
  a translation of the entire Lean language to Metamath. 
  One possible solution would be to reformulate all these conditions in the Lean formalization 
  as inductive types (they can no longer be inductive predicates if we want to extract them, because they won't be large-eliminating)
  that match the Metamath formalization. 
  Any definition as an inductive type is in fact better for extraction than the current definition as boolean functions 
  that some of these condition have. 
  That would require a large refactor of the Matching Logic formalization in Lean, which is undesirable. 
  
  The solution we choose is due to the Matching Logic theorem prover 
  Thus, we chose to simply disregard the Lean proofs of these conditions and generate corresponding 
  Metamath proof from scratch. This is feasible to do in this case since we are working with concrete patterns
  and it is always decidable whether such a condition holds. 
  Furthermore, the task of generating such proofs was already accomplished by Zhengyao Lin 
  in a interactive theorem prover for Matching Logic \cite{Lin20} (???is there a paper to cite???) built on top of the Metamath formalization we target.
  The reference theorem prover has automatic procedures for producing proofs for all the five points we mentioned,
  including a method of generating proofs for any propositional tautology, arguably the most difficult of the tasks.
  Instead of reimplementing all these proof generation procedures, 
  we will call this theorem prover with the appropriate command and include its output proof in the 
  Metamath proof we generate. 

  The core of the interaction with the prover is the \lstinline{runProver} function (showed with simplified type below)
  \begin{lstlisting}
  def runProverOnFile : IO String := do 
    let child ← IO.Process.spawn {
      cmd := "python" -- need a way to decide if python is python3 
      args := #["-m", "ml.itp", toString fname, label]
      stdout := IO.Process.Stdio.piped 
      stdin := IO.Process.Stdio.piped 
    }
  child.stdin.putStrLn command.toString
  child.stdin.putStrLn "proof"
  let stdout ← IO.ofExcept (← IO.asTask child.stdout.readToEnd Task.Priority.dedicated).get
  return extractTheorem stdout 
  \end{lstlisting} 
  which calls the prover on a file containing an unproved statement, and returns the output of the prover. 
  This interaction is complicated by the fact that the prover does not appear to have been designed 
  for a programmatic usage, so we need to pipe to its \texttt{stdin} and \texttt{stdout} when 
  we spawn the process from Lean. 
  The function \lstinline{runProver (statement : Statement 𝕊) : IO String} is then the main API, which bundles the creation of 
  a temporary file with the desired statement and the prover call.


  Freshness and positivity conditions have clear correspondents to Metamath statements.
  Patterns substitutions are exported in a similar manner, though the situation is bit more complicated.
  Where the previous were simple true or false statements, in order to translate a substitution 
  \lstinline{φ[X ⇐ˢ ψ]} we need to produce both a proof that \lstinline{φ[X ⇐ˢ ψ]} is a pattern 
  and a Metamath proof of the statement that \lstinline{φ[X ⇐ˢ ψ]} is the substitution of \lstinline{X} by 
  \lstinline{ψ} in \lstinline{φ}. Again this is decidable, 
  so if \lstinline{X}, \lstinline{φ} and \lstinline{ψ} are all given concretely we can produce 
  all the Metamath proofs needed. 

  Application context also fall in this category of metaconditions. 
  While in Lean, we have defined a inductive type for contexts, 
  the Metamath formalization defines \emph{the propery of some pattern being an application context in some variable}.
  Essentially, a pattern is said to be an application context in that sense, 
  if and only if it is of the form $C[X]$, for some application context (in the Lean formalization sense)
  and for some fresh set variable $X$. Thus, we carry out this conversion when needed, 
  and we are left to generate a proof of its \emph{contextness}, which we defer to the Python prover.

  We mention that the interaction with the theorem prover is a work in progress,
  with only the tautology automation fully working. 
  




  \section{Shape.lean}

  As a refinement of the extraction mechanism, one may wish not to unfold proofs up to 
  primitive rules, but up to some known intermediate lemmas. 
  For example, a theorem \lstinline{thm : φ ⇒ φ} may have some complicated proof, or maybe a proof by tautology.
  In any case, the Metamath formalization contains the theorem \texttt{imp-reflexivity}, 
  which states just that. Thus we may prefer to extract \lstinline{thm} simply to \texttt{imp-reflexivity}
  whenever encountered, without further unfolding. 
  While this would be technically straightforward using metaprogramming as in Chapter \ref{chap:generic-extraction},
  here we do not have access to the name of the declaration. 
  Instead, we identify such theorems based on their statement, so that any term of type 
  \lstinline{φ ⇒ φ} (here \lstinline{φ} stands for a concrete pattern, not a pattern variable!) 
  will be extracted as \texttt{imp-reflexivity}. 
  Same goes for other theorems, e.g. \lstinline{(φ₁ ⇒ φ₂) ⇒ (φ₂ ⇒ φ₃) ⇒ (φ₁ ⇒ φ₃)}.

  For this, we need a way of saying that a pattern has a certain shape, 
  with the holes in the shape filled by some concrete patterns. 
  For example, we would conceptually have shapes \lstinline{?φ ⇒ ?φ} and \lstinline{(?φ₁ ⇒ ?φ₂) ⇒ (?φ₂ ⇒ ?φ₃) ⇒ (?φ₁ ⇒ ?φ₃)}.
  Then, a concrete pattern would need to be matched against one of this shapes, 
  the result, if it matches, being the instantiation of the holes. 
  For example, \lstinline{(?φ₁ ⇒ ?φ₂) ⇒ (?φ₂ ⇒ ?φ₃) ⇒ (?φ₁ ⇒ ?φ₃)} would match against \lstinline{(X ⇒ Y) ⇒ (Y ⇒ Z) ⇒ (X ⇒ Z)}, with 
  \lstinline{?φ₁ := X}, \lstinline{?φ₂ := Y}, \lstinline{?φ₃ := Z}. This is essentially the solving of unification problems for some metavariables,
  but reified at the object level.

  We implement this through the notion of \lstinline{Shape}, 
  \begin{lstlisting}
  def Shape (𝕊 : Type) := 
    Pattern 𝕊 → Option ((List (Σ (α : Type) (_ : ToMMProof α), α)) × String)
  \end{lstlisting}
  which essentially a function describing whether a pattern has a certain shape, 
  and returning the appropriate instantiate together with the Metamath label to be used in the translation.
  The definition is a bit involved because different shapes may have different numbers of holes,
  and the holes may be of different types, therefore we need a heterogenous list. 

  The actual implementation of the transitivity of implication above is 
  \begin{lstlisting}
  def impTransitivity : Shape 𝕊 := 
    fun φ => match φ with 
    | (φ₁ ⇒ φ₂) ⇒ (φ₂' ⇒ φ₃) ⇒ (φ₁' ⇒ φ₃') => 
      if φ₁ = φ₁' ∧ φ₂ = φ₂' ∧ φ₃ = φ₃' then 
        some ⟨[ℍ φ₁, ℍ φ₂, ℍ φ₃], "imp-transitivity"⟩
      else none 
    | _ => none 
  \end{lstlisting}


  Currently, we implemented a list of basic propositional shapes, which are to be exported as such to Metamath,
  and they are a default argument to the main \lstinline{toMMProof} function.





















  \chapter{Translating generic proofs through metaprogramming}\label{chap:generic-extraction}

  Let us go back to the problem described in the beginning of Chpater \ref{chap:concrete-extraction}
  and consider that we are not content with extracting a Metamath proof for each concrete 
  instantiation of a Lean proof. After all, the proof of, say, \lstinline{φ ⇒ φ} is written 
  generically in Lean and we can also write it generically in Metamath. 

  The problem is that in order to perform the extraction generically, 
  we need to access the \lstinline{Proof (φ ⇒ φ)} in the type of \lstinline{implSelf : ∀ φ : Pattern, Proof (φ ⇒ φ)}.
  This ``looking inside the binder'' can only be done by applying it to an argument.
  While this is the case at the level of Lean terms, we can go higher and look at \lstinline{∀ φ : Pattern, Proof (φ ⇒ φ)}
  just as an expression, and by manipulating it as such it is clear that we can get to \lstinline{Proof (φ ⇒ φ)} as a subexpression 
  (in which \lstinline{φ} is now a free variable or a variation thereof, as we will see). 
  Working with Lean expression this way is called metaprogramming. 
  Luckily for us, Lean comes out of the box with a powerful metaprogramming framework,
  which even constitutes one of the important selling points of the language. 

  From all this, a different approach for Metamath extraction emerges: translating generic proofs to generic proofs through metaprogramming.
  This is approach is more brittle and more challenging, both conceptually and technically than the concrete translation, 
  but offers greater flexibility in how and what proofs are translating.

  The core part of the implementation of this approach will be, roughly speaking, 
  a mapping \lstinline{Expr → MMProof} which takes a Lean expression assumed to be a generic Matching Logic proof 
  and produces a corresponding generic Metamath proof.

  We describe in the following the main parts of this implementation. 
  Note that metaprogramming is strictly speaking involved only at some points.
  Also, emitting Metamath reuses the functions from \texttt{MMBuilder.file}, 
  so we do not describe that part again.

  \section{MLParser.lean}
  The \texttt{MLParser.lean} file contains the main pipeline going from a Lean declaration holding 
  a Matching Logic proof and ending with a Metamath proof of the same statement. 
  Roughly speaking we move as in the sequence below. 
  \begin{lstlisting}
Name → Expr → MLTheorem → IRTheorem → MMTheorem → MMFile 
  \end{lstlisting}
  Here, \lstinline{Name} and \lstinline{Expr} are types through which Lean internally represents 
  names and expressions, \lstinline{MLTheorem} is a structure holding a Matching Logic theorem 
  as an \lstinline{Expr} with some other information about it, \lstinline{IRTheorem} (where ``IR'' stands for \emph{intermediate representation}) 
  contains a Matching Logic proof in structured format, as resulting from parsing it 
  from the \lstinline{Expr}'s. The \lstinline{MMTheorem} and \lstinline{MMFile} types have already been discussed in Chapter \ref{chap:emitting-metamath}.

  It is worth noting that many of the functions happen inside the \lstinline{MetaM} monad,
  in which we have access to several very useful meta-operations, like type inference or telescopes.
  
  An \lstinline{MLTheorem} simply contains an expression intended to be a proof, its conclusion as an expression,
  which is actually just the type of \lstinline{proof}, and a name. 
\begin{lstlisting}
structure MLTheorem where 
  name : Option Name := none 
  conclusion : Expr 
  proof : Expr
\end{lstlisting}

Given \lstinline{declName : Name}, \lstinline{parseMLTheorem declName} gets the definition 
with that name and returns a \lstinline{MLTheorem} with that definition as its \lstinline{proof},
the type of \lstinline{proof} as its \lstinline{conclusion} and \lstinline{declName} as its \lstinline{name}.

\begin{lstlisting}
  parseMLTheorem (declName : Name) : MetaM MLTheorem
\end{lstlisting}

  There is one tricky point in implementing \lstinline{parseMLTheorem}: 
  one needs to get the right part of the expression and in the right format. 
  That means we need the subexpression instead all function binders and with all 
  bound variable consistently replaced by metavariables. We do this by $\eta$-expanding, 
  telescoping and reducing to a weak-head normal form: 
  \begin{lstlisting}
  let defnValue ← getDefnValue declName
  let defnValue ← etaExpand defnValue 
  let ⟨_, _, body⟩ ← lambdaMetaTelescope defnValue 
  let type ← inferType <| ← whnf body 
  \end{lstlisting}

  The remaining transformations on \lstinline{MLTheorem}'s, to the \texttt{IR} and then to the \texttt{MM} levels
  are just pointwise transformations of the fields of \lstinline{MLTheorem}, which are described in the next sections.

  \section{IRPatt.lean}

  The type \lstinline{IRPatt} is similar to \lstinline{Pattern},
  but represents patterns up to ``exportable constructs''. 
  By this we mean constructs that we should treat individually in the translation,
  and that we should not or cannot be reduce any further.
  Note that this includes more than pattern constructors, 
  for example derived connectives like \lstinline{or} which possess a Metamath representation.
  Also \lstinline{subst}, which abstractly represents a substitution, 
  is a constructor for \lstinline{IRPatt}, even though it is nontrivial to export it.
  However, there is in general no further reduction possible when encountering a substitition,
  justifying its inclusion in \lstinline{IRPatt}.
  \begin{lstlisting}
inductive IRPatt : Type where 
| metavar : Metavar → IRPatt 
| var : IRPatt → IRPatt 
| bot : IRPatt
| imp : IRPatt → IRPatt → IRPatt 
| app : IRPatt → IRPatt → IRPatt 
| and : IRPatt → IRPatt → IRPatt 
| or : IRPatt → IRPatt → IRPatt 
| not : IRPatt → IRPatt 
| exist : IRPatt → IRPatt → IRPatt 
| all : IRPatt → IRPatt → IRPatt 
| mu : IRPatt → IRPatt → IRPatt
| nu : IRPatt → IRPatt → IRPatt
| subst : IRPatt → IRPatt → IRPatt → IRPatt
| wrong (msg : String := "") : IRPatt
  \end{lstlisting}

  The \lstinline{wrong} constructor is just for a convenient representation of parse errors 
  (remember that we work with arbitrary \lstinline{Expr}'s and thus have no guarantee that they are patterns).
  The difference between \lstinline{var} and \lstinline{metavar} is the following.
  A \lstinline{var} is pattern made of a normal Matching Logic (element or set) variable.
  A \lstinline{metavar} is a pattern metavariable: it will represent the \lstinline{φ} from 
  \lstinline{Proof (φ ⇒ φ)} we discussed at the start of this chapter. 

  The mapping 
\begin{lstlisting}
  IRPatt.fromExpr! (e : Expr) : MetaM IRPatt
\end{lstlisting}
  parses an expression into an \lstinline{IRPatt}. 
  This is done by recursion on subexpressions, treating various cases. 
  For example, a Matching Logic implication is parsed with 
  \begin{lstlisting}
| .app (.app (.app (.const ML.Pattern.implication _) _) p₁) p₂ => 
  let p₁ ← IRPatt.fromExpr! p₁  
  let p₂ ← IRPatt.fromExpr! p₂
  return .imp p₁ p₂
  \end{lstlisting}
  Crucially, \lstinline{fromExpr!} maps Lean metavariables to the \lstinline{metavar} constructor,
  thus bringing the genericity of the proof from the meta to the object level. 
  A \lstinline{metavar} also stores some additional information in its \lstinline{kind}, 
  which can be one of 
  \begin{lstlisting}
inductive MetavarKind where 
| pattern | evar | svar | wrong 
  \end{lstlisting}
  and which is computed based on the type of the initial Lean metavariable. 

  As in the concrete version, we also implement functions that map an \lstinline{IRPatt} to a \lstinline{MMProof},
  to a Metamath statement and that create an environment in which that \lstinline{IRPatt} makes sense. 

  \section{IRProofStructured.lean}

  The main definition in this file is that of the type \lstinline{IRProof}.
  Its goal is to store a parsed Matching Logic proof expression, and is similar in most aspects to \lstinline{IRPatt},
  with constructors for each proof construct to be exported without further unfolding. 
  We again define a recursive parsing function 
  \begin{lstlisting}
    IRProof.fromExpr! (e : Expr) (reducing : Bool := true) : MetaM IRProof 
  \end{lstlisting}
  and \lstinline{toMMProof} and \lstinline{createEnv} functions for intermediate representation proofs. 
  Just like an \lstinline{MMTheorem} at the Metamath-facing level, 
  the structure \lstinline{IRTheorem} stores a proof with its statement, environment and label:
  \begin{lstlisting}
  structure IRTheorem where 
    label : String 
    proof : IRProof 
    conclusion : IRPatt 
    env : Env 
  \end{lstlisting}
  It can be converted to a \lstinline{MMTheorem} by pointwise conversion of the required fields.
  \begin{lstlisting}
def toMMTheorem (thm : IRTheorem) : MMTheorem := 
  {
    label := thm.label 
    proof := thm.proof.toMMProof
    conclusion := thm.conclusion.toClaim
    env := thm.env  
  }
  \end{lstlisting}

  With everything in place, a start-to-finish extraction example can be implemented as 
  \begin{lstlisting}
def exampleExtraction : MetaM Unit := do 
  let mlThm ← parseMLTheorem Tests.modusPonensTest4
  let irThm ← mlThm.toIRTheorem 
  let mmThm := irThm.toMMTheorem 
  let mmFile : MMFile := .fromMMTheorems [mmThm]
  mmFile.writeToFile "example-file.mm"

#eval exampleExtraction
  \end{lstlisting}
  Here, \lstinline{Tests.modusPonensTest4} is a Matching Logic theorem: 

%   \begin{lstlisting}
% def modusPonensTest4 (h₁ : Γ ⊢ φ) (h₂ : Γ ⊢ φ ⇒ ψ) (h₃ : Γ ⊢ ψ ⇒ χ) := 
%   modusPonens (modusPonens h₁ h₂) h₃
%   \end{lstlisting}

  The call produces a file with the following content
  \begin{verbatim}
$[ mm/matching-logic.mm $]
$v  ph19 ph20 ph21 $v.

ph19-is-pattern $f #Pattern ph19 $.
ph20-is-pattern $f #Pattern ph20 $.
ph21-is-pattern $f #Pattern ph21 $.
${
_uniq.24 $e ( \imp ph20 ph21 ) $.
_uniq.23 $e ( \imp ph19 ph20 ) $.
_uniq.22 $e ph19 $.
ML.Meta.Tests.modusPonensTest4 $p |- ph21 $=  
ph20-is-pattern  ph21-is-pattern  ph19-is-pattern  ph20-is-pattern  
_uniq.22  _uniq.23 proof-rule-mp  _uniq.24 proof-rule-mp $.
$}
  \end{verbatim}


  \section{Other files}
  Beyond the main files, other files contain utility definitions or abandoned approaches
  which might be useful later. 

  \texttt{Tests.lean} contains various tests for the metaprogramming extraction. 
  \texttt{ExporterGeneric.lean} implements an alternative to \texttt{IRProofStructured} 
  where one forgets about the structure of the primitive proof rules of the system. 
  Instead, we consider having an expandable dictionary that maps Lean declaration to how they 
  are to be translated to Metamath. The benefit of this would be the ease of exporting theorems 
  up to intermediate lemmas, instead of primitive rules. The implementation for this idea is just a stub. 
  \texttt{Attributes.lean} is related to this approach. A user would be able to tag a Lean declaration with an 
  attribute that specifies how the declaration is to be exported to Metamath 
  (similar in some sense to an \texttt{extern} tag), and then 
  the extraction would behave accordingly, exporting occurences of that declaration straight away,
  instead of further unfolding it. 
  Finally, \texttt{Util.lean} contains a few definitions for which no good place was found.


































\begin{thebibliography}{10}

% \bibitem{CheRos19}
% X. Chen, G. Ro\c{s}u.
% {Matching $\mu$-logic}. In: {\em 34th Annual ACM/IEEE Symposium on Logic in
% Computer Science, LICS 2019, Vancouver, BC, Canada, June 24-27}, 
% pages 1-13, IEEE, 2019.

\bibitem{CheLucRos21}
X. Chen, D. Lucanu, G. Ro\c{s}u. 
{\em Matching logic explained}. 
Journal of Logical and Algebraic Methods in Programming, 120:100638, 2021.

\bibitem{CheMac22}
H. Cheval, B. Macovei.
{\em Matching Logic in Lean}. \\ \url{https://gitlab.com/ilds/aml-lean/MatchingLogic/-/tree/no-wf/}, 2022

\bibitem{BerCheHorPenTus22}
P. Bereczky, X. Chen, D. Horp\'{a}csi, L. Pe\~{n}a, J. Tu\v{s}il.
{\em Mechanizing Matching Logic In Coq}.
arXiv:2201.05716 [cs.LO], 2022.

\bibitem{Lin20}
Z.Lin. \\url{https://github.com/kframework/proof-generation/tree/main/ml/itp}, 2020.

\end{thebibliography}



\end{document}

