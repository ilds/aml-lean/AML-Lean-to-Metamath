import MMExtraction.ConcreteExporter 

namespace ML 

structure Test (𝕊 : Type) (Γ : Premises 𝕊) where 
  claim : Pattern 𝕊 
  proof : Proof Γ claim
  comment : String := ""

deriving instance Repr for Empty 
deriving instance Repr for Pattern 

variable {𝕊 : Type} [DecidableEq 𝕊] [ToMMClaim 𝕊] [Repr 𝕊] {Γ : Premises 𝕊} [ExportablePremises Γ]

def runTests (tests : List <| Test 𝕊 Γ) : IO Unit := do 
  for ⟨claim, proof, comment⟩ in tests do 
    let result ← extractProofToFileAndVerify proof "test" "test.mm" mmknife
    match result with 
    | .ok true => continue
    | .ok false => IO.println <| repr claim 
    | .error msg => IO.println <| repr claim 
  

def freshnessTests : List <| Test Empty ∅ := [
  ⟨∃∃ x₀ (x₁) ⇒ (x₁), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩,
  ⟨∃∃ x₀ (X₀) ⇒ (X₀), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩,
  ⟨∃∃ x₀ (⊥) ⇒ (⊥), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩,
  ⟨∃∃ x₀ (⊥ ⇒ ⊥) ⇒ (⊥ ⇒ ⊥), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩,
  ⟨∃∃ x₀ (⊥ ⬝ ⊥ ⇒ x₁ ⋀ x₁) ⇒ (⊥ ⬝ ⊥ ⇒ x₁ ⋀ x₁), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩,
  ⟨∃∃ x₀ (∃∃ x₁ x₁ ⇒ ⊤) ⇒ (∃∃ x₁ x₁ ⇒ ⊤), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩,
  ⟨∃∃ x₀ (μ X₀ X₀) ⇒ (μ X₀ X₀), .existGen (by simp) (@Proof.implSelf _ _ _), ""⟩
]


#eval runTests freshnessTests



inductive ABC where 
  | a | b | c 
  deriving DecidableEq, Repr 

instance : ToMMClaim ABC where 
  toMMClaim σ := match σ with | .a => "a" | .b => "b" | .c => "c"

open ABC in 
def symbolTests : List <| Test ABC ∅ := [
  ⟨.symbol a ⇒ .symbol a, .implSelf, ""⟩
]

#eval runTests symbolTests

-- def substitutionTests1 : Tests ABC ∅ := 
--   let a : Pattern ABC := .symbol .a 
--   ⟨_, _⟩ 
--   :: []

def substitutionTests2 : List <| Test ABC ∅ := 
  let a : Pattern ABC := .symbol .a
  ⟨a ⇒ ∃∃ x₀ (a), @Proof.existQuan ABC _ (a) x₀ x₁ (by simp), ""⟩ ::
  ⟨x₁ ⇒ ∃∃ x₀ x₀, @Proof.existQuan ABC _ x₀ x₀ x₁ (by simp), ""⟩ :: 
  ⟨(x₁ ⇒ a) ⇒ (∃∃ x₀ ((x₀ ⇒ a))), @Proof.existQuan ABC _ (x₀ ⇒ a) x₀ x₁ (by simp), ""⟩ ::
  ⟨(x₁ ⬝ a) ⇒ (∃∃ x₀ ((x₀ ⬝ a))), @Proof.existQuan ABC _ (x₀ ⬝ a) x₀ x₁ (by simp), ""⟩ ::
  ⟨(a ⬝ x₁) ⇒ (∃∃ x₀ ((a ⬝ x₀))), @Proof.existQuan ABC _ (a ⬝ x₀) x₀ x₁ (by simp), ""⟩ ::
  ⟨(∃∃ x₀ a) ⇒ (∃∃ x₀ (∃∃ x₀ a)), @Proof.existQuan ABC _ (∃∃ x₀ a) x₀ x₁ (by simp), ""⟩ ::
  ⟨(∃∃ x₁ a) ⇒ (∃∃ x₀ (∃∃ x₁ a)), @Proof.existQuan ABC _ (∃∃ x₁ a) x₀ x₁ (by simp), ""⟩ :: 
  let C : AppContext ABC := .left ⊥ □; ⟨C[x₁] ⇒ ∃∃ x₀ (C[x₀]), @Proof.existQuan ABC _ (C[x₀]) x₀ x₁ (by simp), ""⟩ ::
  let C : AppContext ABC := .right ⊥ □; ⟨C[x₁] ⇒ ∃∃ x₀ (C[x₀]), @Proof.existQuan ABC _ (C[x₀]) x₀ x₁ (by simp), ""⟩ ::
  let C : AppContext ABC := .right (.symbol .b) (.left (.symbol .a) (.right ⊥ □)); ⟨C[x₁] ⇒ ∃∃ x₀ (C[x₀]), @Proof.existQuan ABC _ (C[x₀]) x₀ x₁ (by simp), ""⟩ 
  :: []

#eval runTests substitutionTests2    

open AppContext in 
def contextTests : List <| Test ABC ∅ := [
  ⟨(left (.symbol .a) empty)[⊥] ⇒ ⊥, .propagationBottomLeft, ""⟩,
  ⟨(right (.symbol .a) empty)[⊥] ⇒ ⊥, .propagationBottomRight, ""⟩, 
  ⟨(⊥ ⬝> (⊥ ⬝> □))[⊥] ⇒ ⊥, .propagationBottom, ""⟩,
  ⟨((⊥ ⬝> □) <⬝ ⊥)[⊥] ⇒ ⊥, .propagationBottom, ""⟩,
  ⟨(.symbol .a ⋁ .symbol .b) ⬝ .symbol .c ⇒ (.symbol .a ⬝ .symbol .c) ⋁ (.symbol .b ⬝ .symbol .c), .propagationDisjRight, ""⟩,
  ⟨.symbol .c ⬝ (.symbol .a ⋁ .symbol .b) ⇒ (.symbol .c ⬝ .symbol .a) ⋁ (.symbol .c ⬝ .symbol .b), .propagationDisjLeft, ""⟩,
  let C : AppContext ABC := (□ <⬝ (.symbol .c)) <⬝ (.symbol .c) ; ⟨C[.symbol .a ⋁ .symbol .b] ⇒ (C[Pattern.symbol .a]) ⋁ C[Pattern.symbol .b], .propagationDisj, ""⟩
]

#eval runTests contextTests 


-- @[simp] def ΓABC : Premises ABC := {
--   .symbol .b,
--   .symbol .c
-- }

-- instance : ExportablePremises ΓABC where 
--   axioms := [.mkAxiom "ax-b" "|- b", .mkAxiom "ax-c" "|-c"]
--   premiseSkeleton φ hmem := 
--     match φ with 
--     | .symbol .b => { label := "ax-b", bones := []}
--     | .symbol .c => { label := "ax-c", bones := []}
--     | _ => False.elim sorry

-- def symbolWithPremisesTests : List <| Test ABC ΓABC := [
--   ⟨.symbol .b, .premise (by simp) (by simp), ""⟩
-- ]

-- #eval runTests symbolWithPremisesTests


-- @[simp] def Γ₀ : Premises Empty := { φ ⬝ φ | φ : Pattern Empty }
