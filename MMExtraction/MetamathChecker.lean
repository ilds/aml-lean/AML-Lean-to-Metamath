

abbrev Checker := System.FilePath → IO Bool

def mmknife (path : System.FilePath := "metamath-knife") : Checker :=
  fun fname => do 
    let output ← IO.Process.output {
      cmd := toString path 
      args := #["--verify", toString fname]
    }
    return output.exitCode == 0