import Lean
import Mathlib
import MMExtraction.Util

set_option autoImplicit false 

namespace ML.Metamath

/--
  The kind of a Metamath hypothesis, which can be either 
  * `floating` or 
  * `essential`.
-/
inductive HypKind where 
| floating | essential | «axiom» 
  deriving DecidableEq, Inhabited, Repr

instance : ToString HypKind where 
  toString k := match k with 
  | .floating => "f"
  | .essential => "e"
  | .«axiom» => "a"

abbrev Claim := String 

abbrev Label := String

/--
  A Metamath hypothesis made of
  * `label : String` 
  * `stmt : List String`
  * `kind : HypKind`.
  The intended meaning is for such an object to represent the Metamath statement of the form 
  `"label $kind stmt $."` where `kind` will be wither `e`, `f` or `a`.
-/
structure Hypothesis where 
  label : Label 
  stmt : Claim 
  kind : HypKind
  deriving DecidableEq, Inhabited, Repr

namespace Hypothesis

  def mkFloating (label : Label) (stmt : Claim) : Hypothesis := 
    ⟨label, stmt, .floating⟩

  def mkEssential (label : Label) (stmt : Claim) : Hypothesis := 
    ⟨label, stmt, .essential⟩

  def mkAxiom (label : Label) (stmt : Claim) : Hypothesis := 
    ⟨label, stmt, .«axiom»⟩

  /--
  Prints a Metamath hypotheses into Metamath format, i.e. 
  `"label $kind stmt $."` where `kind` will be wither `e`, `f` or `a`.
  -/
  def toMM (hyp : Hypothesis) : String := 
    s! "{hyp.label} ${hyp.kind} {hyp.stmt} $."

end Hypothesis



instance : ToString Hypothesis := ⟨Hypothesis.toMM⟩

structure Metavar where 
  name : String 
  floating : Hypothesis 
  deriving DecidableEq, Inhabited, Repr 

def Metavar.equalNames : Metavar → Metavar → Bool := (name . == name .)

def Metavar.toMM (mv : Metavar) : String := 
  s!"$v {mv.name} $. {endl} {mv.floating.toMM}"

structure Constant where 
  name : String 
  deriving DecidableEq, Inhabited, Repr 

/--
  Holds the environment of a Metamath theorem and its proof, containing:
  * `metavars` - the list of mentioned necessary variables 
  * `«axiom»` - the axiom (i.e. `$a` statements) required by the theorem 
-/
structure TopEnv where 
  metavars : Array Metavar := #[]
  constants : Array Constant := #[]
  axioms : Array Hypothesis := #[]
  deriving DecidableEq, Inhabited, Repr

abbrev TopEnvM := StateM TopEnv 

abbrev EmittingM := ExceptT String TopEnvM 

namespace TopEnv 

  protected def toString (env : TopEnv) : String := 
    s! "metavars: {repr env.metavars} {endl}" ++ 
    s! "constants: {repr env.constants} {endl}" ++
    s! "assumtpions: {env.axioms} {endl}" 

  instance : ToString TopEnv := ⟨TopEnv.toString⟩

  def toMM (env : TopEnv) : String := Id.run do 
    let mut mm := ""
    for metavar in env.metavars do 
      mm := mm ++ metavar.toMM ++ endl
    let disjoint := 
      if env.metavars.size > 1 then 
        "$d " ++ (env.metavars.map Metavar.name |>.joinWith " ") ++ " $."
      else ""
    let constantsStr := if env.constants.isEmpty then "" else "$c " ++ (env.constants.map Constant.name |>.joinWith " ") ++ " $."
    let axiomsStr := env.axioms.map Hypothesis.toMM |>.joinWith endl 
    return mm ++ endl ++ disjoint ++ endl ++ constantsStr ++ endl ++ axiomsStr ++ endl

  def addMetavar (mv : Metavar) : TopEnvM Unit := 
    modify <| fun env => { env with metavars := env.metavars.insertP (Metavar.equalNames) mv }
  
  def addAxiom (label : String) (stmt : Claim) : TopEnvM Unit := 
    modify <| fun env => { env with axioms := env.axioms.insert <| .mkAxiom label stmt }

  def addAxiomHyp (hyp : Hypothesis) : TopEnvM Unit := 
    modify <| fun env => { env with axioms := env.axioms.insert hyp }

  def addConstant (const : Constant) : TopEnvM Unit :=
    modify <| fun env => { env with constants := env.constants.insert const }

  def addVar (name : String) : TopEnvM Unit := 
    addMetavar ⟨name, .mkFloating s!"{name}-is-var" s! "#Variable {name}"⟩

  def addElementVar (name : String) : TopEnvM Unit := 
    addMetavar ⟨name, .mkFloating s!"{name}-is-element-var" s!"#ElementVariable {name}"⟩ 

  def addSetVar (name : String) : TopEnvM Unit := 
    addMetavar ⟨name, .mkFloating s!"{name}-is-set-var" s!"#SetVariable {name}"⟩

  def addPattern (name : String) : TopEnvM Unit := 
    addMetavar ⟨name, .mkFloating s!"{name}-is-pattern" s!"#Pattern {name}"⟩

  def addSymbol (name : String) : TopEnvM Unit := do
    addConstant ⟨name⟩ 
    addAxiom s!"{name}-is-symbol" s!"#Symbol {name}"
  
  def containsMetavar (env : TopEnv) : Metavar → Bool := 
    env.metavars.contains 

end TopEnv 


inductive MMProof where 
| app (head : String) (args : Array MMProof) (comment : Option String := none) : MMProof 
| incomplete 
  deriving BEq, Inhabited, Repr 

def commentToMM (comment : Option String) : String :=
    match comment with 
    | some comment => s!" $( {comment} $)"
    | none => ""

partial def MMProof.toMM (proof : MMProof) (indentation : String := "")
  : String := 
  match proof with 
  | app head args comment => 
    Array.joinWith (args.map <| MMProof.toMM (indentation := "  " ++ indentation)) " " 
      ++ endl 
      ++ indentation 
      ++ head 
      ++ (commentToMM comment)
  | incomplete => "?"

inductive MMTheoremKind where
  | logical | positive | negative | fresh | substitution | context 
  deriving DecidableEq, Inhabited, Repr 

def MMTheoremKind.toLeadingToken : MMTheoremKind → String 
  | logical => "|-"
  | positive => "#Positive"
  | negative => "#Negative"
  | fresh => "#Fresh"
  | substitution => "#Substitution"
  | context => "#ApplicationContext"


structure MMTheorem where 
  label : Label 
  proof : MMProof 
  conclusion : Claim 
  kind : MMTheoremKind := .logical
  deriving BEq, Inhabited, Repr

namespace MMTheorem 

  def toMM (thm : MMTheorem) : String := 
    s!"{thm.label} $p {thm.kind.toLeadingToken} {thm.conclusion} $= {thm.proof.toMM} $. {endl}"

  -- def containsMetavar (thm : MMTheorem) : Metavar → Bool := 
  --   thm.env.containsMetavar

  -- def toMM (thm : MMTheorem) : TopEnvM String := 
  --   let metavarsStr : String := thm.env.metavars.map Metavar.name |>.foldl (init := "") (.++" "++.)
  --   let floatingsStr := thm.env.metavars.map Metavar.floating |>.map Hypothesis.toMM |>.foldl (init := "") (.++endl++.)
  --   let essentialsStr := thm.env.essentials.map Hypothesis.toMM |>.foldl (init := "") (.++endl++.)
    
  --   let ⟨beginScope, endScope⟩ : String × String := 
  --     if essentialsStr.length > 0 then ⟨"${", "$}"⟩ else ⟨"", ""⟩

  --   (if !thm.env.metavars.isEmpty then s!"$v {metavarsStr} $." else "") ++ "\n" 
  --     ++ (if thm.env.metavars.size > 1 then s!"$d {metavarsStr} $." else "") ++ "\n"
  --     ++ floatingsStr ++ endl
  --     ++ beginScope 
  --     ++ essentialsStr ++ endl
  --     ++ thm.label ++ " "
  --     ++ s! "$p {thm.kind.toLeadingToken} {toString thm.conclusion} $= {thm.proof.toMM} $." ++ endl
  --     ++ endScope

  -- def containsMetavar (thm : MMTheorem) : Metavar → Bool := 
  --   thm.env.containsMetavar



end MMTheorem


structure MMFile where 
  topenv : TopEnv 
  theorems : List MMTheorem 
  includes : List System.FilePath := ["matching-logic-aux.mm", "matching-logic-prelude-lemmas.mm"]
  deriving BEq, Inhabited, Repr

namespace MMFile 

  def toMM (file : MMFile) : String :=
    let includesStr := 
      file.includes
      |>.map (fun filename => s! "$[ {filename} $]")
      |>.foldl (init := "") (.++endl++.)
    includesStr ++ endl 
      ++ file.topenv.toMM ++ endl
      ++ (file.theorems.map MMTheorem.toMM |>.foldl (.++endl++.) "")

  def writeToFile (mmfile : MMFile) (fname : System.FilePath) : IO Unit := do 
    IO.FS.writeFile fname mmfile.toMM

end MMFile 


