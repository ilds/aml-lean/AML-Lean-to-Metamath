import MatchingLogic 
import MMExtraction.ConcreteExporter.ToMMProof 

namespace ML 

open Metamath 

structure Bone where 
  {Carrier : Type}
  [carrierToMMProof : ToMMProof Carrier]
  (value : Carrier)

protected def Bone.toMMProof (bone : Bone) (comment : Option String := none) : EmittingM MMProof := 
  @toMMProof _ bone.carrierToMMProof bone.value comment 

instance : ToMMProof Bone where 
  toMMProof := (Bone.toMMProof . .)

structure Skeleton where 
  bones : List Bone 
  label : String 

def Skeleton.toMMProof (skeleton : Skeleton) : EmittingM MMProof := 
  do return .app skeleton.label ⟨← skeleton.bones.mapM Bone.toMMProof⟩

def SkeletonMatcher (𝕊 : Type) := Pattern 𝕊 → Option Skeleton 

namespace Skeleton 

-- /-- 
--   `ℍ t` maps `t : α` into `⟨α, inferInstance, t⟩ : Σ (α : Type) (_ : ?c), t` 
--   where `?c` should be a typeclass already inferrable from the context. 
--   Useful for constructing heterogenous collections where all elements 
--   are of types satisfying the same typeclass.
-- -/
-- macro "ℍ" t:term : term => `(⟨_, inferInstance, $t⟩)

variable [DecidableEq 𝕊] [ToMMClaim 𝕊]

def impReflexivity : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | φ₁ ⇒ φ₁' => if φ₁ = φ₁' then return ⟨[⟨φ₁⟩], "imp-reflexivity"⟩ else none 
  | _ => none

def impTransitivity : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | (φ₁ ⇒ φ₂) ⇒ (φ₂' ⇒ φ₃) ⇒ (φ₁' ⇒ φ₃') => 
    if φ₁ = φ₁' ∧ φ₂ = φ₂' ∧ φ₃ = φ₃' then 
      some ⟨[⟨φ₁⟩, ⟨φ₂⟩, ⟨φ₃⟩], "imp-transitivity"⟩
    else none 
  | _ => none 

def botElim : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | ⊥ ⇒ φ₁ => some ⟨[⟨φ₁⟩], "bot-elim"⟩
  | _ => none 

def contradiction : SkeletonMatcher 𝕊 :=
  fun φ => match φ with 
  | (∼φ₁ ⇒ ⊥) ⇒ φ₁' => 
    if φ₁ = φ₁' then 
      some ⟨[⟨φ₁⟩], "contradiction"⟩
    else none 
  | _ => none 

def topIntro : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | ⊤ => some ⟨[⟨(⊤ : Pattern 𝕊)⟩], "top-intro"⟩
  | _ => none 

def mp : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | φ₀ ⇒ (φ₀' ⇒ φ₁) ⇒ φ₁' => 
    if φ₀ = φ₀' ∧ φ₁ = φ₁' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "mp"⟩
    else none 
  | _ => none 

def contrapositive : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | (φ₀ ⇒ φ₁) ⇒ (∼φ₁' ⇒ ∼φ₀') => 
    if φ₀ = φ₀' ∧ φ₁ = φ₁' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "contrapositive-sugar"⟩
    else none 
  | _ => none 

def andElimLeft : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | (φ₀ ⋀ φ₁) ⇒ φ₀' => 
    if φ₀ = φ₀' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "and-elim-left"⟩
    else none 
  | _ => none 

def andElimRight : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | (φ₀ ⋀ φ₁) ⇒ φ₁' => 
    if φ₁ = φ₁' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "and-elim-right"⟩
    else none 
  | _ => none 

def andIntro : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | φ₀ ⇒ φ₁ ⇒ (φ₀' ⋀ φ₁') => 
    if φ₀ = φ₀' ∧ φ₁ = φ₁' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "and-intro-sugar"⟩
    else none 
  | _ => none 

def orIntroLeft : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | φ₀ ⇒ (φ₀' ⋁ φ₁) =>  
    if φ₀ = φ₀' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "or-intro-left"⟩ 
    else none 
  | _ => none


def orIntroRight : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | φ₁ ⇒ (φ₀ ⋁ φ₁') =>  
    if φ₁ = φ₁' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "or-intro-right"⟩ 
    else none 
  | _ => none


def orElim : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | (φ₀ ⇒ φ₂) ⇒ (φ₁ ⇒ φ₂') ⇒ (φ₀' ⋁ φ₁' ⇒ φ₂'') =>
    if φ₀ = φ₀' ∧ φ₁ = φ₁' ∧ φ₂ = φ₂' ∧ φ₂ = φ₂'' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩, ⟨φ₂⟩], "or-elim-sugar"⟩
    else none 
  | _ => none 

def andCommutativity : SkeletonMatcher 𝕊 := 
  fun φ => match φ with 
  | φ₀ ⋀ φ₁ ⇒ φ₁' ⋀ φ₀' => 
    if φ₀ = φ₀' ∧ φ₁ = φ₁' then 
      some ⟨[⟨φ₀⟩, ⟨φ₁⟩], "and-commutativity"⟩
    else none 
  | _ => none 

def standardPropositional : List (SkeletonMatcher 𝕊) := [
  impReflexivity, 
  impTransitivity, 
  botElim, 
  contradiction,
  topIntro,
  mp,
  contrapositive,
  andElimLeft,
  andElimRight,
  andIntro,
  orIntroLeft,
  orIntroRight,
  orElim,
  andCommutativity
]

def axiomDefinedness : SkeletonMatcher Definedness := 
  fun φ => match φ with 
  | .symbol Definedness.defined ⬝ .evar x => some ⟨[⟨x⟩], "axiom-definedness"⟩ -- this require for `defined` to be specially exported as `\ceil` 
  | _ => none 



/-

  imp-refl : φ ⇒ φ 
  φ ⇒ ψ ⇒ φ 

  ⊥ ⇒ ⊥ 
-/