import MMExtraction.ConcreteExporter.Substitution 

set_option autoImplicit false 

namespace ML 

inductive ApplicationContext (𝕊 : Type) where
| var (xX : Var) 
| appLeft (xX : Var) (c : Pattern 𝕊) (φ : Pattern 𝕊) : ApplicationContext 𝕊 → ApplicationContext 𝕊
| appRight (xX : Var) (c : Pattern 𝕊) (φ : Pattern 𝕊) : ApplicationContext 𝕊 → ApplicationContext 𝕊

variable {𝕊 : Type}

-- φ₁ X (φ₂ X)