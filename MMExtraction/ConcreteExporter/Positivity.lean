import MatchingLogic 
import MMExtraction.MMBuilder 
import MMExtraction.ConcreteExporter.Var

namespace ML 

variable {𝕊 : Type} 

mutual 
  /--
    `Positive xX φ` is an inductive representation of a Metamath proof that 
    `φ` is positive for `xX`. 
  -/
  inductive Positive (xX : Var) : Pattern 𝕊 → Type where 
  | disjoint {φ} : ¬φ.isVar xX → Positive xX φ
  | var (yY : Var) (φ) : Positive xX φ
  | symbol (σ : 𝕊) : Positive xX (.symbol σ)
  | bot : Positive xX ⊥
  | app {φ₁ φ₂ : Pattern 𝕊} : Positive xX φ₁ → Positive xX φ₂ → Positive xX (φ₁ ⬝ φ₂)
  | imp {φ₁ φ₂ : Pattern 𝕊} : Negative xX φ₁ → Positive xX φ₂ → Positive xX (φ₁ ⇒ φ₂)
  | exist (x : EVar) {φ : Pattern 𝕊} : Positive xX φ → Positive xX (∃∃ x φ)
  | mu (X : SVar) {φ : Pattern 𝕊} : Positive xX φ → Positive xX (μ X φ)

  /--
    `Negative xX φ` is an inductive representation of a Metamath proof that 
    `φ` is negative for `xX`. 
  -/
  inductive Negative (xX : Var) : Pattern 𝕊 → Type where 
  | disjoint {φ} : ¬φ.isVar xX → Negative xX φ
  | var {yY : Var} (φ) : xX ≠ yY → Negative xX φ
  | symbol (σ : 𝕊) : Negative xX (.symbol σ)
  | bot : Negative xX ⊥
  | app {φ₁ φ₂ : Pattern 𝕊} : Negative xX φ₁ → Negative xX φ₂ → Negative xX (φ₁ ⬝ φ₂)
  | imp {φ₁ φ₂ : Pattern 𝕊} : Positive xX φ₁ → Negative xX φ₂ → Negative xX (φ₁ ⇒ φ₂)
  | exist (x : EVar) {φ : Pattern 𝕊} : Negative xX φ → Negative xX (∃∃ x φ)
  | mu (X : SVar) {φ : Pattern 𝕊} : Negative xX φ → Negative xX (μ X φ)
end 

mutual /- `autoPositive` `autoNegative`-/
  -- these are not partial, but I don't care about their termination for the time being 
  /--
    `autoPositive xX φ` produces a Metamath proof that `φ` is a positive for `xX` represented 
    through the `Positive` type if that's the case, and `none` otherwise.
  -/
  partial def autoPositive (xX : Var) (φ : Pattern 𝕊) : Except String (Positive xX φ) := do 
    if h : ¬φ.isVar xX then 
      return .disjoint h
    else match φ with 
    -- | .evar x => return .app "positive-in-var" [toMMProof xX, toMMProof x] 
    | .evar x => return .var x (.evar x)
    | .svar X => return .var X (.svar X)
    | .symbol σ => return .symbol σ
    | ⊥ => return .bot 
    | φ₁ ⇒ φ₂ => return .imp (← autoNegative xX φ₁) (← autoPositive xX φ₂)
    | φ₁ ⬝ φ₂ => return .app (← autoPositive xX φ₁) (← autoPositive xX φ₂)
    | ∃∃ x ψ => return .exist x (← autoPositive xX ψ) 
    | μ X ψ => return .mu X (← autoPositive xX ψ)

  /--
    `autoNegative xX φ` produces a Metamath proof that `φ` is a negative for `xX` represented 
    through the `Negative` type if that's the case, and `none` otherwise.
  -/
  partial def autoNegative (xX : Var) (φ : Pattern 𝕊) : Except String (Negative xX φ) := do 
    if h : ¬φ.isVar xX then 
      return .disjoint h
    else match φ with 
    | .evar x => 
      if h' : xX ≠ x then 
        return .var x h' 
      else .error "" 
    | .svar X => 
      if h' : xX ≠ X then 
        return .var X h'
      else .error ""
    | .symbol σ => return .symbol σ
    | ⊥ => return .bot 
    | φ₁ ⇒ φ₂ => return .imp (← autoPositive xX φ₁) (← autoNegative xX φ₂)
    | φ₁ ⬝ φ₂ => return .app (← autoNegative xX φ₁) (← autoNegative xX φ₂)
    | ∃∃ x ψ => return .exist x (← autoNegative xX ψ) 
    | μ X ψ => return .mu X (← autoNegative xX ψ)
end 