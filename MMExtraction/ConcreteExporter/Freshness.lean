import MatchingLogic 
import MMExtraction.MMBuilder 
import MMExtraction.ConcreteExporter.Var

namespace ML 

variable {𝕊 : Type} [Repr 𝕊]

deriving instance Repr for Pattern

/--
  `Fresh xX φ` is an inductive representation of a Metamath proof 
  that `xX` is fresh in `φ`. 
-/
inductive Fresh (xX : Var) : Pattern 𝕊 → Type where 
| var {yY : Var} : xX ≠ yY → Fresh xX yY.toPattern 
| symbol (σ : 𝕊) : Fresh xX (.symbol σ)
| bot : Fresh xX (⊥ : Pattern 𝕊)
| imp {φ ψ : Pattern 𝕊} : Fresh xX φ → Fresh xX ψ → Fresh xX (φ ⇒ ψ)
| app {φ ψ : Pattern 𝕊} : Fresh xX φ → Fresh xX ψ → Fresh xX (φ ⬝ ψ)
| exist {x : EVar} {φ : Pattern 𝕊} : xX ≠ x → Fresh xX φ → Fresh xX (∃∃ x φ)
| existShadowed {x : EVar} (φ : Pattern 𝕊) : xX = x → Fresh xX (∃∃ x φ)
| mu {X : SVar} {φ : Pattern 𝕊} : xX ≠ X → Fresh xX φ → Fresh xX (μ X φ)
| muShadowed {X : SVar} (φ : Pattern 𝕊) : xX = X → Fresh xX (μ X φ)


/--
  `autoFresh xX φ` produces a Metamath proof that `xX` is fresh in `φ` represented  
  through the `Fresh` type if that's the case, and `none` otherwise.
-/
def autoFresh (xX : Var) (φ : Pattern 𝕊) : Except String (Fresh xX φ) := do 
  match φ with 
  | .evar x =>
    if h : xX ≠ x then return .var h
    else .error "" 
  | .svar X =>  
    if h : xX ≠ X then return .var h
    else .error ""
  | .symbol σ => return .symbol σ
  | ⊥ => return .bot 
  | φ₁ ⇒ φ₂ => return .imp (← autoFresh xX φ₁) (← autoFresh xX φ₂)
  | φ₁ ⬝ φ₂ => return .app (← autoFresh xX φ₁) (← autoFresh xX φ₂)
  | ∃∃ x ψ => 
    if h : xX ≠ x then 
      return .exist h (← autoFresh xX ψ)
    else 
      return .existShadowed ψ (by simp_all)
  | μ X ψ => 
    if h : xX ≠ X then 
      return .mu h (← autoFresh xX ψ) 
    else 
      return .muShadowed ψ (by simp_all)

