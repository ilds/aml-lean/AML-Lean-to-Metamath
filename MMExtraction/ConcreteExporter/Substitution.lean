import MatchingLogic 
import MMExtraction.ConcreteExporter.Freshness
import MMExtraction.ConcreteExporter.ToMMClaim
import MMExtraction.MMBuilder 

set_option autoImplicit false

namespace ML 

variable {𝕊 : Type} 

inductive Substitution (𝕊 : Type) where 
| varSame (xX : Var) (φ₀ : Pattern 𝕊) 
| varDiff (xX : Var) (φ₀ : Pattern 𝕊) (yY : Var)
| symbol (var : Var) (substituent : Pattern 𝕊) (σ : 𝕊)
| bot (var : Var) (substituent : Pattern 𝕊) 
| imp (var : Var) (substituent : Pattern 𝕊) (φ₁ φ₂ : Pattern 𝕊) (s₁ s₂ : Substitution 𝕊) 
| app (var : Var) (substituent : Pattern 𝕊) (φ₁ φ₂ : Pattern 𝕊) (s₁ s₂ : Substitution 𝕊) 
| existShadowed (substituent : Pattern 𝕊) (x : EVar) (φ : Pattern 𝕊)
| exist (var : Var) (substituent : Pattern 𝕊) (x : EVar) (φ : Pattern 𝕊) (s : Substitution 𝕊)
| muShadowed (substituent : Pattern 𝕊) (X : SVar) (φ : Pattern 𝕊)
| mu (var : Var) (substituent : Pattern 𝕊) (X : SVar) (φ : Pattern 𝕊) (s : Substitution 𝕊)
| identity (xX : Var) (φ₀ : Pattern 𝕊)
| fresh (xX : Var) (substituent : Pattern 𝕊) (target : Pattern 𝕊) (hfresh : Fresh xX target)


/--
  `autoSubstitution xX ψ φ` produces a Metamath proof that `φ[xX ⇐ ψ]` is the free substitution of 
  `xX` by `ψ` in `φ`, or returns `none` if such a substitution is not free. 

  Unlike the Metamath definition, `autoSubstitution` does **not** perform α-renaming to make 
  all substitutions capture-avoiding. One should do this in Lean **before** attemtping to extract the proof.

  #Substitution target[var ⇐ substituent] target var substituent
-/
def autoSubstitution (var : Var) (substituent : Pattern 𝕊) (target : Pattern 𝕊) : Except String <| Substitution 𝕊 := 
do 
  if let .ok fresh := autoFresh var target then 
    return .fresh var substituent target fresh

  if let some identity := 
    match var, substituent with 
    | .evar x, .evar y => if x = y then some <| .identity x target else none 
    | .svar X, .svar Y => if X = Y then some <| .identity X target else none 
    | _, _ => none 
  then 
    return identity 

  match target with 
  | .evar x =>
    if var = x then 
      return .varSame var substituent 
    else 
      return .varDiff var substituent x 
  | .svar X => 
    if var = X then 
      return .varSame var substituent 
    else 
      return .varDiff var substituent X
  | .symbol σ => 
    return .symbol var substituent σ
  | ⊥ => 
    return .bot var substituent 
  | φ₁ ⇒ φ₂ => 
    let s₁ ← autoSubstitution var substituent φ₁ 
    let s₂ ← autoSubstitution var substituent φ₂
    return .imp var substituent φ₁ φ₂ s₁ s₂
  | φ₁ ⬝ φ₂ => 
    let s₁ ← autoSubstitution var substituent φ₁ 
    let s₂ ← autoSubstitution var substituent φ₂
    return .app var substituent φ₁ φ₂ s₁ s₂
  | ∃∃ x φ => 
    if var = x then 
      return .existShadowed substituent x φ
    else if .evar x ∉ φ.allVars then 
      let s ← autoSubstitution var substituent φ
      return .exist var substituent x φ s
    else .error "Substitution is not caputer-avoiding, so it could not be exported" 
  | μ X φ => 
    if var = X then 
      return .muShadowed substituent X φ
    else if .svar X ∉ φ.allVars then 
      let s ← autoSubstitution var substituent φ
      return .mu var substituent X φ s
    else .error "Substitution is not caputer-avoiding, so it could not be exported" 
    

#reduce @autoSubstitution Empty (.evar ⟨0⟩) (.evar ⟨1⟩) (.evar ⟨0⟩ ⇒ .evar ⟨0⟩)