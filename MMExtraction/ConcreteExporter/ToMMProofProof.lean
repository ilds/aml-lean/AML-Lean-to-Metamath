import MatchingLogic 
import MMExtraction.ConcreteExporter.Shape 
import MMExtraction.ConcreteExporter.ToMMProof

set_option autoImplicit false 
set_option linter.unusedVariables.patternVars false

namespace ML 

open Metamath

variable {𝕊 : Type} [ToMMClaim 𝕊] [DecidableEq 𝕊]

def boxSVar := SVar.mk 999



def Option.lift : MonadLift Option (OptionT TopEnvM) where 
  monadLift x := OptionT.mk <| pure x
-- attribute [instance] Option.lift in

class ExportablePremises (Γ : Premises 𝕊) where 
  axioms : List Hypothesis 
  premiseSkeleton : ∀ φ ∈ Γ, Skeleton 

instance {𝕊} : ExportablePremises (∅ : Premises 𝕊) where 
  axioms := []
  premiseSkeleton _ := False.elim 

-- we need to put `toMMClaim` in `TopEnvM` for this. 
-- it is probably worth it
def Metamath.TopEnv.addLogicalAxiom {𝕊 : Type} [ToMMClaim 𝕊] (label : Label) (φ : Pattern 𝕊) : TopEnvM Unit := do 
  addAxiom label φ.toMMClaim


protected partial def Proof.toMMProof {Γ : Premises 𝕊} [ExportablePremises Γ] {φ : Pattern 𝕊} (proof : Proof Γ φ)
  (matchers : List <| SkeletonMatcher 𝕊 := Skeleton.standardPropositional)
  (comment : String := "")
  : EmittingM MMProof := 
do 
  for «axiom» in ExportablePremises.axioms Γ do 
    TopEnv.addAxiomHyp «axiom»

  for matcher in matchers do 
    if let some skeleton := matcher φ then 
      return ← skeleton.toMMProof -- why can't I just `skeleton.toMMProof`; don't `return` and `←` cancel???? I became lost in the monad forest!!!

  match proof with 
  | @tautology _ _ φ _ => 
    Except.error "unexportable tautology encountered"
  | @premise _ _ φ _ hmem => 
    let skeleton := ExportablePremises.premiseSkeleton φ hmem 
    return ← skeleton.toMMProof 
  | @modusPonens _ _ φ ψ h₁ h₂ => 
    return .app "proof-rule-mp" #[
      ← φ.toMMProof "ph0",
      ← ψ.toMMProof "ph1 ",
      ← h₂.toMMProof matchers "proof-rule-mp.1",
      ← h₁.toMMProof matchers "proof-rule-mp.0"
    ] "proof-rule-mp" 
  | @existQuan _ _ φ x y sfi => 
    return .app "proof-rule-exists" #[
      ← φ[x ⇐ᵉ y].toMMProof "ph0",
      ← φ.toMMProof "ph1 ",
      ← x.toMMProof "x",
      ← y.toMMProof "y ",
      ← (← autoSubstitution x y φ).toMMProof "proof-rule-exists.0"
    ] "proof-rule-exists"
  | @existGen _ _ φ ψ x nfv h => 
    return .app "proof-rule-gen" #[
      ← φ.toMMProof "ph0",
      ← ψ.toMMProof "ph1",
      ← x.toMMProof "x",
      ← h.toMMProof matchers "proof-rule-gen.0",
      ← (← autoFresh (.evar x) ψ).toMMProof "proof-rule.gen.1"
    ] "proof-rule-gen"
  | @existence _ _ x => 
    return .app "proof-rule-existence" #[
      ← x.toMMProof " "
    ] "proof-rule-existence"
  | @propagationBottomLeft _ _ c => 
    let X := boxSVar
    let C : AppContext 𝕊 := c ⬝> □
    return .app "proof-rule-propagation-bot" #[
      ← C[X].toMMProof "ph0",
      ← C[⊥].toMMProof "ph1",
      ← (Var.svar X).toMMProof "xX",
      ← C.toMMProofIn X,
      ← (← autoSubstitution X ⊥ (C[X])).toMMProof "proof-rule-propagation-bot.0"
    ] "proof-rule-propagation-bot"
  | @propagationBottomRight _ _ c => 
    let X := boxSVar
    let C : AppContext 𝕊 := □ <⬝ c
    return .app "proof-rule-propagation-bot" #[
      ← C[X].toMMProof "ph0",
      ← C[⊥].toMMProof "ph1",
      ← (Var.svar X).toMMProof "xX",
      ← C.toMMProofIn X,
      ← (← autoSubstitution X ⊥ (C[X])).toMMProof "proof-rule-propagation-bot.0"
    ] "proof-rule-propagation-bot"
  | @propagationDisjLeft _ _ φ ψ c => 
    let X : SVar := boxSVar
    let C : AppContext 𝕊 := c ⬝> □
    return .app "proof-rule-propagation-or-desugared" #[
      ← C[X].toMMProof "ph0",
      ← C[φ ⋁ ψ].toMMProof "ph1",
      ← C[φ].toMMProof "ph2",
      ← C[ψ].toMMProof "ph3",
      ← φ.toMMProof "ph4",
      ← ψ.toMMProof "ph5 ",
      ← (Var.svar X).toMMProof "xX",
      ← C.toMMProofIn X "proof-rule-propagation-or.0",
      ← (← autoSubstitution X (φ ⋁ ψ) (C[X])).toMMProof "proof-rule-propagation-or.1",
      ← (← autoSubstitution X φ (C[X])).toMMProof "proof-rule-propagation-or.2",
      ← (← autoSubstitution X ψ (C[X])).toMMProof "proof-rule-propagation-or.3 "
    ] "proof-rule-propagation-or-desugared"
  | @propagationDisjRight _ _ φ ψ c => 
    let X : SVar := boxSVar
    let C : AppContext 𝕊 := □ <⬝ c
    return .app "proof-rule-propagation-or-desugared" #[
      ← C[X].toMMProof "ph0",
      ← C[φ ⋁ ψ].toMMProof "ph1",
      ← C[φ].toMMProof "ph2",
      ← C[ψ].toMMProof "ph3",
      ← φ.toMMProof "ph4",
      ← ψ.toMMProof "ph5 ",
      ← (Var.svar X).toMMProof "xX",
      ← C.toMMProofIn X "proof-rule-propagation-or.0",
      ← (← autoSubstitution X (φ ⋁ ψ) (C[X])).toMMProof "proof-rule-propagation-or.1",
      ← (← autoSubstitution X φ (C[X])).toMMProof "proof-rule-propagation-or.2 ",
      ← (← autoSubstitution X ψ (C[X])).toMMProof "proof-rule-propagation-or.3"
    ] "proof-rule-propagation-or-desugared"
  | @propagationExistLeft _ _ φ x c nfv => 
    let X : SVar := boxSVar 
    let C : AppContext 𝕊 := c ⬝> □ 
    return .app "not-implemented" #[
      ← C[X].toMMProof "ph0",
      ← C[∃∃ x φ].toMMProof "ph1",
      ← C[φ].toMMProof "ph2",
      ← φ.toMMProof "ph3",
      ← x.toMMProof "y",
      ← (Var.svar X).toMMProof "xX",
      ← C.toMMProofIn X "proof-rule-propagation-exists.0",
      ← (← autoSubstitution X (∃∃ x φ) (C[X])).toMMProof "proof-rule-propagation-exists.1",
      ← (← autoSubstitution X φ (C[X])).toMMProof "proof-rule-propagation-exists.2",
      ← (← autoFresh x (C[X])).toMMProof "proof-rule-propagation-exists.3"
    ] comment 
  | @propagationExistRight _ _ φ x c nfv => 
    let X : SVar := boxSVar 
    let C : AppContext 𝕊 := □ <⬝ c 
    return .app "not-implemented" #[
      ← C[X].toMMProof "ph0",
      ← C[∃∃ x φ].toMMProof "ph1",
      ← C[φ].toMMProof "ph2",
      ← φ.toMMProof "ph3",
      ← x.toMMProof "y",
      ← (Var.svar X).toMMProof "xX",
      ← C.toMMProofIn X "proof-rule-propagation-exists.0",
      ← (← autoSubstitution X (∃∃ x φ) (C[X])).toMMProof "proof-rule-propagation-exists.1",
      ← (← autoSubstitution X φ (C[X])).toMMProof "proof-rule-propagation-exists.2",
      ← (← autoFresh x (C[X])).toMMProof "proof-rule-propagation-exists.3"
    ] comment 
  | @framingLeft _ _ φ ψ c h => 
    let X := boxSVar
    let C := □ <⬝ c
    return .app "proof-rule-frame" #[
      ← C[X].toMMProof "ph0",
      ← C[φ].toMMProof "ph1",
      ← C[ψ].toMMProof "ph2",
      ← φ.toMMProof "ph3" ,
      ← ψ.toMMProof "ph4",
      ← (Var.svar X).toMMProof "xX ",
      ← C.toMMProofIn X,
      ← (← autoSubstitution X φ (C[X])).toMMProof "proof-rule-frame.1",
      ← (← autoSubstitution X ψ (C[X])).toMMProof "proof-rule-frame.2",
      ← h.toMMProof matchers "proof-rule-frame.3"                                                  
    ] "proof-rule-frame"
  | @framingRight _ _ φ ψ c h => 
    let X := boxSVar
    let C := c ⬝> □
    return .app "proof-rule-frame" #[
      ← C[X].toMMProof "ph0",
      ← C[φ].toMMProof "ph1",
      ← C[ψ].toMMProof "ph2",
      ← φ.toMMProof "ph3" ,
      ← ψ.toMMProof "ph4",
      ← (Var.svar X).toMMProof "xX ",
      ← C.toMMProofIn X,
      ← (← autoSubstitution X φ (C[X])).toMMProof "proof-rule-frame.1",
      ← (← autoSubstitution X ψ (C[X])).toMMProof "proof-rule-frame.2",
      ← h.toMMProof matchers "proof-rule-frame.3"                                                  
    ] "proof-rule-frame"
  | @substitution _ _ φ ψ X sfi h => 
    return .app "proof-rule-set-var-substitution" #[
      ← φ[X ⇐ˢ ψ].toMMProof "ph0",
      ← φ.toMMProof "ph1",
      ← ψ.toMMProof "ph2 ",
      ← X.toMMProof "X",
      ← (← autoSubstitution X ψ φ).toMMProof "proof-rule-set-var-substitution.0",
      ← h.toMMProof matchers "proof-rule-set-var-substitution.1"
    ] "proof-rule-set-var-substitution"
  | @prefixpoint _ _ φ X pos sfi => 
    return .app "proof-rule-prefixpoint" #[
      ← φ.toMMProof,                                                      
      ← X.toMMProof, 
      ← (← autoPositive (.svar X) φ).toMMProof
    ] "proof-rule-prefixpoint"
  | @knasterTarski _ _ φ ψ X sfi h => 
    return .app "proof-rule-kt" #[
      ← φ[X ⇐ˢ ψ].toMMProof,
      ← φ.toMMProof, 
      ← ψ.toMMProof, 
      ← X.toMMProof, 
      ← (← autoSubstitution X ψ φ).toMMProof,
      ← h.toMMProof matchers
    ] "proof-rule-kt"
  | @Proof.singleton _ _ C₁ C₂ x φ => 
    let X : SVar := boxSVar 
    let Y : SVar := boxSVar
    return .app "proof-rule-singleton" #[
      ← toMMProof <| C₁.insert X,                                                       -- ph0
      ← toMMProof <| C₂.insert X,                                                       -- ph1 
      ← toMMProof φ,                                                                    -- ph2 
      ← toMMProof <| C₁.insert <| x ⋀ φ,                                                -- ph3
      ← toMMProof <| C₂.insert <| x ⋀ ∼φ,                                               -- ph4
      ← toMMProof x,                                                                    -- x
      ← toMMProof <| Var.svar X,                                                        -- xX
      ← toMMProof <| Var.svar Y                                                        -- yY   
      -- .app (Statement.context (.svar X) (C₁.insert X) |>.toLabel) #[],                 -- proof-rule-singleton.0
      -- .app (Statement.context (.svar Y) (C₂.insert Y) |>.toLabel) #[],                 -- proof-rule-singleton.1
      -- .app (Statement.substitution (.svar X) (x ⋀ φ) (C₁.insert X) |>.toLabel) #[],    -- proof-rule-singleton.2
      -- .app (Statement.substitution (.svar Y) (x ⋀ ∼φ) (C₂.insert Y) |>.toLabel) #[]    -- proof-rule-singleton.3
    ] "proof-rule-singleton"


def th₁ : ∅ ⊢ (⊥ ⇒ ⊥ : Pattern Empty) := Proof.implSelf
def th₀ : ∅ ⊢ (∃∃ ⟨0⟩ ⊥ ⇒ ⊥ : Pattern Empty) := Proof.existGen (by simp) th₁
