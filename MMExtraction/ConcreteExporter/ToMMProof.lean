import MatchingLogic 
import MMExtraction.MMBuilder
import MMExtraction.ConcreteExporter.ToMMClaim
import MMExtraction.ConcreteExporter.Var
import MMExtraction.ConcreteExporter.Freshness 
import MMExtraction.ConcreteExporter.Positivity 
import MMExtraction.ConcreteExporter.Substitution
import MMExtraction.ConcreteExporter.ToLabel 
import MMExtraction.ConcreteExporter.PatternDestruct

set_option autoImplicit false 
set_option linter.unusedVariables.patternVars false

namespace ML 

open ML.Metamath
open TopEnv

class ToMMProof (α : Type _) where 
  toMMProof : α → (comment : Option String := none) → EmittingM MMProof
export ToMMProof (toMMProof)

instance : ToMMProof Empty := ⟨Empty.rec⟩

instance {𝕊 : Type} [ToMMClaim 𝕊] : ToMMProof 𝕊 where 
  toMMProof σ comment := do 
    addSymbol <| toMMClaim σ
    return .app s!"{toMMClaim σ}-is-symbol" #[] comment 

protected def EVar.toMMProof (x : EVar) (comment : Option String := none): EmittingM MMProof := do
  addElementVar x.toMMClaim
  return .app s! "{x.toMMClaim}-is-element-var" #[] comment

instance : ToMMProof EVar where toMMProof := (EVar.toMMProof . .)

protected def SVar.toMMProof (X : SVar) (comment : Option String := none) : EmittingM MMProof := do
  addSetVar X.toMMClaim
  return .app s! "{X.toMMClaim}-is-set-var" #[] comment 

instance : ToMMProof SVar where toMMProof := (SVar.toMMProof . .)

protected def Var.toMMProof (xX : Var) (comment : Option String := none) : EmittingM MMProof := do
  match xX with 
  | .evar x => return .app "element-var-is-var" #[← toMMProof x] comment 
  | .svar X => return .app "set-var-is-var" #[← toMMProof X] comment 

instance : ToMMProof Var := ⟨(Var.toMMProof . .)⟩

variable {𝕊 : Type} [ToMMProof 𝕊]

-- obviously terminating, but the termination checks goes over the heartbeat limit 
-- the big increase in time happens after adding the `∼φ` pattern 
protected partial def Pattern.toMMProof (φ : Pattern 𝕊) (comment : Option String := none)
  : EmittingM MMProof := 
do
-- We cannot define this with the custom `@[match_pattern]` as commented out below 
-- because that makes typechecking take extremly long (letting the function be a normal causes `maxHeartbeats` to be exceeded 
-- and even making it `partial` takes long at this makes further definition take even longer. For example, even 
-- with `partial`, `Proof.toMMProof` takes so long that I have never seen the end of its typechecking. )
-- | φ ⋁ ψ => .app "or-is-pattern" [φ.toMMProof, ψ.toMMProof]
-- | φ ⋀ ψ => .app "and-is-pattern" [φ.toMMProof, ψ.toMMProof]
-- | ∀∀ x φ => .app "forall-is-pattern" [x.toMMProof, φ.toMMProof]
-- | ⊤ => .app "top-is-pattern" []
-- | ∼φ => .app "not-is-pattern" [φ.toMMProof]

-- Instead we do the following ugly pattern matching using some custom destructuring. 
  -- if let some _        := φ.getTopArgs         then return .app "top-is-pattern"    #[]
  -- if let some (φ₁, φ₂) := φ.getConjunctionArgs then return .app "and-is-pattern"    #[← φ₁.toMMProof, ← φ₂.toMMProof]
  -- if let some (φ₁, φ₂) := φ.getDisjunctionArgs then return .app "or-is-pattern"     #[← φ₁.toMMProof, ← φ₂.toMMProof]
  -- if let some ψ        := φ.getNegationArgs    then return .app "not-is-pattern"    #[← ψ.toMMProof]
  -- if let some (x, ψ)   := φ.getUniversalArgs   then return .app "forall-is-pattern" #[← x.toMMProof, ← ψ.toMMProof]
  match φ with 
  | evar x   => 
    return .app "var-is-pattern"    #[.app "element-var-is-var" #[← x.toMMProof] ]
  | svar X   => 
    return .app "var-is-pattern"    #[.app "set-var-is-var" #[← X.toMMProof] ]
  | ⊥        => 
    return .app "bot-is-pattern"    #[]
  | φ ⇒ ψ    => 
    return .app "imp-is-pattern"    #[← φ.toMMProof, ← ψ.toMMProof]
  | φ ⬝ ψ    => 
    return .app "app-is-pattern"    #[← φ.toMMProof, ← ψ.toMMProof]
  | ∃∃ x φ   => 
    return .app "exists-is-pattern" #[← φ.toMMProof, ← x.toMMProof] 
  | μ X φ    => 
    return .app "mu-is-pattern"     (#[← φ.toMMProof, ← X.toMMProof])
  | symbol σ => 
    return .app "symbol-is-pattern" #[← toMMProof σ]

instance : ToMMProof <| Pattern 𝕊 := ⟨(Pattern.toMMProof . .)⟩

protected def Fresh.toMMProof {xX : Var} {φ : Pattern 𝕊} (fresh : Fresh xX φ) (comment : Option String := none) : EmittingM MMProof := do 
  match fresh with 
  | @var _ _ yY _ => 
    return .app "fresh-in-var" #[← toMMProof xX, ← toMMProof yY] comment 
  | @symbol _ _ σ => 
    return .app "fresh-in-symbol" #[← toMMProof xX, ← toMMProof σ] comment 
  | @bot _ _ => 
    return .app "fresh-in-bot" #[← toMMProof xX] comment 
  | @imp _ _ φ ψ freshφ freshψ => 
    return .app "fresh-in-imp" #[
      ← toMMProof φ, 
      ← toMMProof ψ, 
      ← toMMProof xX, 
      ← freshφ.toMMProof "fresh-in-imp.0", 
      ← freshψ.toMMProof "fresh-in-imp.0"
    ] comment 
  | @app _ _ φ ψ freshφ freshψ => 
    return .app "fresh-in-app" #[
      ← toMMProof φ,
      ← toMMProof ψ, 
      ← toMMProof xX, 
      ← freshφ.toMMProof "fresh-in-app.0", 
      ← freshψ.toMMProof "fresh-in-app.1"
    ] comment 
  | @exist _ _ x φ _ freshφ => 
    return .app "fresh-in-exists" #[
      ← toMMProof φ, 
      ← toMMProof x, 
      ← toMMProof xX, 
      ← freshφ.toMMProof "fresh-in-exists.0"
    ] comment 
  | @existShadowed _ _ x φ _ => 
    return .app "fresh-in-exists-shadowed" #[
      ← toMMProof φ, 
      ← toMMProof x
    ] comment 
  | @mu _ _ X φ _ freshφ => 
    return .app "fresh-in-mu" #[
      ← toMMProof φ, 
      ← toMMProof X, 
      ← toMMProof xX, 
      ← freshφ.toMMProof "fresh-in-mu.0"
    ] comment 
  | @muShadowed _ _ X φ _ => 
    return .app "fresh-in-mu-shadwoed" #[
      ← toMMProof φ, 
      ← toMMProof X
    ] comment 

instance {xX : Var} {φ : Pattern 𝕊} : ToMMProof <| Fresh xX φ where 
  toMMProof := (Fresh.toMMProof . .)


mutual
  protected partial def Positive.toMMProof {xX : Var} {φ : Pattern 𝕊} (pos : Positive xX φ) (comment : Option String := none) : EmittingM MMProof := 
    do match pos with 
    | .disjoint (φ := φ) _ => 
      return .app "positive-disjoint" #[
        ← toMMProof xX, 
        ← toMMProof φ
      ] comment 
    | @Positive.var _ _ yY φ => 
      return .app "positive-in-var" #[
        ← toMMProof xX, 
        ← toMMProof yY
      ] comment
    | @Positive.symbol _ _ σ => 
      return .app "positive-in-symbol" #[
        ← toMMProof xX, 
        ← toMMProof σ
      ] comment
    | @Positive.bot _ _ => 
      return .app "positive-in-bot" #[
        ← toMMProof xX
      ] comment
    | @Positive.imp _ _ φ₁ φ₂ neg₁ pos₂ => 
      return .app "positive-in-imp" #[
        ← toMMProof xX, 
        ← toMMProof φ₁, 
        ← toMMProof φ₂, 
        ← neg₁.toMMProof "positive-in-imp.0", 
        ← pos₂.toMMProof "positive-in-imp.1"
      ] comment
    | @Positive.app _ _ φ₁ φ₂ pos₁ pos₂ => 
      return .app "positive-in-app" #[
        ← toMMProof xX, 
        ← toMMProof φ₁, 
        ← toMMProof φ₂, 
        ← pos₁.toMMProof "positive-in-app.0", 
        ← pos₂.toMMProof "positive-in-app.1"
      ] comment
    | @Positive.exist _ _ x φ pos => 
      return .app "positive-in-exists" #[
        ← toMMProof xX, 
        ← toMMProof φ, 
        ← pos.toMMProof "positive-in-exists.0"
      ] comment
    | @Positive.mu _ _ X φ pos => 
      return .app "positive-in-mu" #[
        ← toMMProof xX, 
        ← toMMProof φ, 
        ← pos.toMMProof "positive-in-mu.0"
      ] comment

  protected partial def Negative.toMMProof {xX : Var} {φ : Pattern 𝕊} (neg : Negative xX φ) (comment : Option String := none) : EmittingM MMProof := do 
    do match neg with 
    | @Negative.disjoint _ _ φ _ => 
      return .app "negative-disjoint" #[
        ← toMMProof xX, 
        ← toMMProof φ
      ] comment
    | @Negative.var _ _ yY φ _ => 
      return .app "negative-in-var" #[
        ← toMMProof xX, 
        ← toMMProof yY
      ] comment
    | @Negative.symbol _ _ σ => 
      return .app "negative-in-symbol" #[
        ← toMMProof xX, 
        ← toMMProof σ
      ] comment
    | @Negative.bot _ _ => 
      return .app "negative-in-bot" #[
        ← toMMProof xX
      ] comment
    | @Negative.imp _ _ φ₁ φ₂ pos₁ neg₂ => 
      return .app "negative-in-imp" #[
        ← toMMProof xX, 
        ← toMMProof φ₁, 
        ← toMMProof φ₂, 
        ← pos₁.toMMProof "negative-in-imp.0", 
        ← neg₂.toMMProof "negative-in-imp.1"
      ] comment
    | @Negative.app _ _ φ₁ φ₂ neg₁ neg₂ => 
      return .app "negative-in-app" #[
        ← toMMProof xX, 
        ← toMMProof φ₁, 
        ← toMMProof φ₂, 
        ← neg₁.toMMProof "negative-in-app.0", 
        ← neg₂.toMMProof "negative-in-app.1"
      ] comment
    | @Negative.exist _ _ x φ pos => 
      return .app "negative-in-exists" #[
        ← toMMProof xX, 
        ← toMMProof x, 
        ← toMMProof φ, 
        ← pos.toMMProof "negative-in-exists.0"
      ] comment
    | @Negative.mu _ _ X φ pos => 
      return .app "negative-in-mu" #[
        ← toMMProof xX, 
        ← toMMProof X, 
        ← toMMProof φ, 
        ← pos.toMMProof "negative-in-mu.0"
      ] comment
end 

instance {xX : Var} {φ : Pattern 𝕊} : ToMMProof <| Positive xX φ where 
  toMMProof := (Positive.toMMProof . .) 

instance {xX : Var} {φ : Pattern 𝕊} : ToMMProof <| Negative xX φ where 
  toMMProof := (Negative.toMMProof . .) 


/-
  FOR ME: The earlier a floating is declared, the higher it needs to be in the argument stack,
  meaning the more at the beginning of the `MMProof.app` arguments
-/

protected def Substitution.toMMProof (subst : Substitution 𝕊) (comment : Option String := none) : EmittingM MMProof := do 
  match subst with 
  | varSame var substituent => 
    return .app "substitution-var-same" #[
      ← toMMProof substituent, 
      ← toMMProof var
    ] comment
  | varDiff var substituent yY => 
    return .app "substitution-var-diff" #[
      ← toMMProof substituent, 
      ← toMMProof var, 
      ← toMMProof yY
    ] comment
  | symbol var substituent σ => 
    return .app "substitution-var-symbol" #[
      ← toMMProof substituent, 
      ← toMMProof var, 
      ← toMMProof σ
    ] comment
  | bot var substituent => 
    return .app "substitution-bot" #[
      ← toMMProof substituent, 
      ← toMMProof var
    ] comment
  | imp var substituent φ₁ φ₂ s₁ s₂ => 
    return .app "substitution-imp" #[
      ← toMMProof substituent, 
      ← toMMProof (φ₁[var ⇐ substituent]), 
      ← toMMProof (φ₂[var ⇐ substituent]), 
      ← toMMProof φ₁, 
      ← toMMProof φ₂, 
      ← toMMProof var, 
      ← s₁.toMMProof "substitution-imp.0", 
      ← s₂.toMMProof "substitution-imp.1"
    ] comment
  | app var substituent φ₁ φ₂ s₁ s₂ => 
    return .app "substitution-app" #[
      ← toMMProof substituent, 
      ← toMMProof (φ₁[var ⇐ substituent]), 
      ← toMMProof (φ₂[var ⇐ substituent]), 
      ← toMMProof φ₁, 
      ← toMMProof φ₂, 
      ← toMMProof var, 
      ← s₁.toMMProof "substitution-app.0", 
      ← s₂.toMMProof "substitution-app.1"
    ] comment
  | exist var substituent x φ s => 
    return .app "substitution-exists" #[
      ← toMMProof substituent
    ] comment
  | existShadowed substituent x φ =>   
    return .app "substitution-exists-shadowed" #[
      ← toMMProof substituent, 
      ← toMMProof φ, 
      ← toMMProof x
    ] comment
  | mu var substituent X φ s => 
    return .app "substitution-mu" #[
    
    ] comment
  | muShadowed substituent X φ =>   
    return .app "substitution-mu-shadowed" #[
      ← toMMProof substituent, 
      ← toMMProof φ, 
      ← toMMProof X
    ] comment
  | fresh xX substituent target hfresh =>  
    return .app "substitution-fresh" #[
      ← toMMProof target, 
      ← toMMProof substituent, 
      ← toMMProof xX, 
      ← toMMProof hfresh "substitution-fresh.0"
    ] comment
  | identity xX φ => 
    return .app "substitution-identity" #[
      ← toMMProof φ, 
      ← toMMProof xX
    ] comment


instance : ToMMProof (Substitution 𝕊) where 
  toMMProof := (Substitution.toMMProof . .)

/-- Notation for `C.right φ` -/
@[for_matching_logic] notation C "<⬝" φ => AppContext.right φ C
/-- Notation for `C.left φ` -/
@[for_matching_logic] notation φ "⬝>" C => AppContext.left φ C

protected def AppContext.toMMProofIn (X : SVar) (C : AppContext 𝕊) (comment : Option String := none) : EmittingM MMProof := do 
  match C with 
  | □ => return .app "application-context-var" #[← Var.svar X |>.toMMProof] comment
  | C' <⬝ χ => return .app "application-context-app-left" #[← C'[X].toMMProof, ← χ.toMMProof, ← Var.svar X |>.toMMProof, ← C'.toMMProofIn X] comment
  | χ ⬝> C' => return .app "application-context-app-right" #[← χ.toMMProof, ← C'[X].toMMProof, ← Var.svar X |>.toMMProof, ← C'.toMMProofIn X] comment

def AppContext.toMMProof (C : AppContext 𝕊) (comment : Option String := none) := AppContext.toMMProofIn ⟨999⟩ C comment 

instance : ToMMProof (AppContext 𝕊) where 
  toMMProof := (AppContext.toMMProof . .)

instance {α : Type} [Repr α] : Repr (Except String α) := inferInstance

#eval show IO Unit from do 
  if let .ok prf := @autoSubstitution Empty (.evar ⟨0⟩) (.evar ⟨1⟩) (.evar ⟨0⟩ ⇒ .evar ⟨0⟩) then 
    if let .ok prf := prf.toMMProof.run {} |>.fst then 
      IO.println <| repr prf
  



 