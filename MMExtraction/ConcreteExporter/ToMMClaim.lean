import MatchingLogic 
import MMExtraction.ConcreteExporter.Var 
import MMExtraction.ConcreteExporter.PatternDestruct

namespace ML

class ToMMClaim (α : Type) where 
  toMMClaim : α → String 
export ToMMClaim (toMMClaim)

instance : ToMMClaim Empty := ⟨Empty.rec⟩

protected def EVar.toMMClaim : EVar → String 
  | ⟨idx⟩ => s! "_x{idx}"

instance : ToMMClaim EVar := ⟨EVar.toMMClaim⟩

protected def SVar.toMMClaim : SVar → String 
  | ⟨idx⟩ => s! "_X{idx}"

instance : ToMMClaim SVar := ⟨SVar.toMMClaim⟩

protected def Var.toMMClaim : Var → String 
  | .evar x => toMMClaim x 
  | .svar X => toMMClaim X 

instance : ToMMClaim Var := ⟨Var.toMMClaim⟩

section variable {𝕊 : Type} [ToMMClaim 𝕊]

  protected partial def Pattern.toMMClaim (φ : Pattern 𝕊) : String :=
  Id.run do 
    -- if let some _        := φ.getTopArgs         then return s!"\\top"
    -- if let some (φ₁, φ₂) := φ.getConjunctionArgs then return s!"( \\and {φ₁.toMMClaim} {φ₂.toMMClaim} )"
    -- if let some (φ₁, φ₂) := φ.getDisjunctionArgs then return s!"( \\or {φ₁.toMMClaim} {φ₂.toMMClaim} )"
    -- if let some ψ        := φ.getNegationArgs    then return s!"( \\not {ψ.toMMClaim} )"
    -- if let some (x, ψ)   := φ.getUniversalArgs   then return s!"( \\forall {x.toMMClaim} {ψ.toMMClaim} )"
    match φ with 
    | evar x => x.toMMClaim
    | svar X => X.toMMClaim
    | ⊥ => "\\bot"
    | φ ⇒ ψ => s! "( \\imp {φ.toMMClaim} {ψ.toMMClaim} )"
    | φ ⬝ ψ => s! "( \\app {φ.toMMClaim} {ψ.toMMClaim} )"
    | ∃∃ x φ => s! "( \\exists {x.toMMClaim} {φ.toMMClaim} )"
    | μ X φ => s! "( \\mu {X.toMMClaim} {φ.toMMClaim} )"
    | symbol σ => toMMClaim σ

  instance : ToMMClaim <| Pattern 𝕊 := ⟨Pattern.toMMClaim⟩

end 