import MatchingLogic 

namespace ML.Pattern  

variable {𝕊 : Type}

def getConjunctionArgs : Pattern 𝕊 → Option (Pattern 𝕊 × Pattern 𝕊)
  | φ ⋀ ψ => some (φ, ψ)
  | _ => none 

def getDisjunctionArgs : Pattern 𝕊 → Option (Pattern 𝕊 × Pattern 𝕊)
  | φ ⋁ ψ => some (φ, ψ)
  | _ => none 

def getNegationArgs : Pattern 𝕊 → Option (Pattern 𝕊)
  | ∼φ => some φ
  | _ => none 

def getExistentialArgs : Pattern 𝕊 → Option (EVar × Pattern 𝕊)
  | ∃∃ x φ => some (x, φ)
  | _ => none 

def getUniversalArgs : Pattern 𝕊 → Option (EVar × Pattern 𝕊)
  | ∀∀ x φ => some (x, φ)
  | _ => none 

def getTopArgs : Pattern 𝕊 → Option Unit 
  | ⊤ => some () 
  | _ => none 
