import MatchingLogic 
import MMExtraction.MMBuilder
import MMExtraction.Attributes
import MMExtraction.ConcreteExporter.ToMMClaim
import MMExtraction.ConcreteExporter.ToMMProofProof 
import MMExtraction.MetamathChecker

set_option autoImplicit false 

namespace ML 

open ML.Metamath 

def x₀ : EVar := ⟨0⟩
def x₁ : EVar := ⟨1⟩
def X₀ : SVar := ⟨0⟩
def X₁ : SVar := ⟨1⟩

#eval (EVar.mk 0 ⇒ EVar.mk 1 ⇒ EVar.mk 2 : Pattern Empty).toMMProof.run {}

def pf : Proof ∅ ((∃∃ ⟨0⟩ (EVar.mk 0)) ⇒ (∃∃ ⟨0⟩ (EVar.mk 0)) : Pattern Empty) := .implSelf

variable {𝕊 : Type} [ToMMClaim 𝕊] [DecidableEq 𝕊] [Repr 𝕊] 

class Exportable (𝕊 : Type) extends ToMMClaim 𝕊, Repr 𝕊

def Proof.toMMTheorem 
  {Γ : Premises 𝕊} [ExportablePremises Γ] {φ : Pattern 𝕊} 
  (proof : Proof Γ φ)
  (label : Label)
  : ExceptT String TopEnvM MMTheorem := 
do 
  return {
    label := label
    proof := (← proof.toMMProof)
    conclusion := φ.toMMClaim
  }

#eval pf.toMMTheorem "" |>.run {}

def Proof.toMMFile 
  {Γ : Premises 𝕊} [ExportablePremises Γ] {φ : Pattern 𝕊}
  (proof : Proof Γ φ)
  (label : Label)
  : Except String MMFile := 
do 
  let ⟨thm?, env⟩ := proof.toMMTheorem label |>.run {}
  return {
    topenv := env 
    theorems := [← thm?] 
  }

#eval pf.toMMFile ""

def extractProofToFile 
  {Γ : Premises 𝕊} [ExportablePremises Γ] {φ : Pattern 𝕊}
  (proof : Proof Γ φ)
  (label : Label)
  (fname : System.FilePath) 
  : IO Bool := 
do 
  if let .ok mmfile := proof.toMMFile label then 
    mmfile.writeToFile fname 
    return true 
  else 
    return false 

#eval extractProofToFile pf "pf" "test.mm"

def extractProofToFileAndVerify 
  {Γ : Premises 𝕊} [ExportablePremises Γ] {φ : Pattern 𝕊}
  (proof : Proof Γ φ)
  (label : Label)
  (fname : System.FilePath)
  (checker : System.FilePath → IO Bool) 
  : IO (Except String Bool) := 
do 
  let extracted ← extractProofToFile proof label fname 
  if extracted then 
    return .ok <| ← checker fname 
  else 
    return .error "No file to verify"

#eval extractProofToFileAndVerify pf "pf" "test.mm" mmknife