A translator from Matching Logic proofs formalized in Lean (https://gitlab.com/ilds/aml-lean/MatchingLogic) to Matching Logic proofs formalized in Metamath (https://github.com/kframework/proof-generation). 

The repository contains actually two implementations: 
* ConcreteExporter, which Γ ⊢ φ with φ instantiated by concrete patterns (e.g. ⊥ ⇒ ⊥)
* GenericExporter (abandoned at the moment), which can translate Γ ⊢ φ leaving patterns as metavariables and translating them to Metamath variables

An end-user is intended to be able to just call `extractProofToFileAndVerify proof label fname checker` (imported from `ConcreteExporter`) 
which will translate `proof` to the Metamath file `fname` as a theorem with name `label`, and check the file with provided Metamath `checker`. 

**Note:** The technical report describes an older version of the implementation and it is no longer representative
