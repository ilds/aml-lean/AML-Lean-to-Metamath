
$[ matching-logic-propositional.mm $]

${
    proof-rule-propagation-or-desugared.0 $e #ApplicationContext xX ph0 $.
    proof-rule-propagation-or-desugared.1 $e #Substitution ph1 ph0 ( \imp ( \imp ph4 \bot ) ph5 ) xX $.
    proof-rule-propagation-or-desugared.2 $e #Substitution ph2 ph0 ph4 xX $.
    proof-rule-propagation-or-desugared.3 $e #Substitution ph3 ph0 ph5 xX $.
    proof-rule-propagation-or-desugared $p |- ( \imp ph1 ( \imp ( \imp ph2 \bot ) ph3 ) ) $= ( imp-is-pattern bot-is-pattern not-is-pattern notation-reflexivity or-is-pattern notation-transitivity notation-imp or-is-sugar not-is-sugar notation-symmetry notation-substitution proof-rule-propagation-or notation-proof ) BCDPLBCMLDLLABCDEFGHBAEMLFLBAEFPGIBOAOEFPENFLEMLFLEFSENFEMLFENEMLEMLETEMLOQFORQUBJKUCBCMLDLBCDPBOCMLDLCNDLCDPCMLDCNDCMLCMLCNCMLOCNCMLCTUAQDORCDPCNDLCDSUAQRUD $.
$}